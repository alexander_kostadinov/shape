"use strict";

$(document).ready(function() {
    $('#btn-login').click(function(e) {
        e.preventDefault();
        var username = $('#username').val();
        var password = $('#password').val();
        doLogin(username, password);
    });
});

function doLogin(username, password) {
    var params = {
        module: 'login',
        unit: 'service',
        method: 'doLogin',
        token: token,
        params: {
            username: username,
            password: password
        }
    };
    
    $.ajax({
        type: 'POST',
        url: AJAX_URL,
        data: 'params=' + JSON.stringify(params),
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        cache: false,
        dataType: 'json'
    }).success(function(data) {
        if (data.username === undefined) {
          //reset field visual validation status
            $('#username-group').removeClass('has-error');
            $('#password-group').removeClass('has-error');
            $('#username-group').addClass('has-success');
            $('#password-group').addClass('has-success');
            $('.error-login-block').each(function() {
                this.remove();
            });
            for (var err in data) {
                switch (data[err]) {
                    case ERROR_EMPTY_USERNAME:
                        $('#username-group').addClass('has-error');
                        $('<span>').addClass('error-login-block').text(ERROR_EMPTY_USERNAME_TEXT).insertBefore('#btn-login');
                        break;
                    case ERROR_EMPTY_PASSWORD:
                        $('#password-group').addClass('has-error');
                        $('<span>').addClass('error-login-block').text(ERROR_EMPTY_PASSWORD_TEXT).insertBefore('#btn-login');
                        break;
                    case EROR_WRONG_CREDENTIALS:
                        $('#username-group').addClass('has-error');
                        $('#password-group').addClass('has-error');
                        $('<span>').addClass('error-login-block').text(EROR_WRONG_CREDENTIALS_TEXT).insertBefore('#btn-login');
                        break;
                }
            }
        } else {
            $('#username-group').removeClass('has-error');
            $('#password-group').removeClass('has-error');
            $('#username-group').removeClass('has-success');
            $('#password-group').removeClass('has-success');
            
            window.location.href = 'index';
            return;
        }
    });
}