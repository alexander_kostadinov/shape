"use strict";

/*
 * Initializes the fast markup redirection 
 */
function initMarkupScrolling() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
            && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $(HTML_TAG_SELECTOR + ',' + BODY_TAG_SELECTOR).animate({
                  scrollTop: target.offset().top
                }, 1000);
                
                return false;
            }
        }
    });
}

/*
 * Initializes the back-to-top functionality
 */
function initBackToTopButton() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $(GO_TO_TOP_BTN_SELECTOR).fadeIn(500);
        } else {
            $(GO_TO_TOP_BTN_SELECTOR).fadeOut(300);
        }
    });
    
    $(GO_TO_TOP_BTN_SELECTOR).click(function(event) {
        event.preventDefault();

        $(HTML_TAG_SELECTOR + ',' + BODY_TAG_SELECTOR).animate({scrollTop: 0}, 300);
    });
}

/*
 * Makes certain elements fluid (on window resize)
 */
function makeElementsFluid() {
    if ($(window).width() < 450) {
        $(GO_TO_TOP_BTN_SELECTOR).html(ENTITY_UP_ARROW);
    }
    
    $(window).resize(function() {
        if ($(window).width() < 450) {
            $(GO_TO_TOP_BTN_SELECTOR).html(ENTITY_UP_ARROW);
        } else {
            $(GO_TO_TOP_BTN_SELECTOR).html(GO_TO_TOP_BTN_TXT);
        }
    });
    
    $(IMG_TAG_SELECTOR).attr(ATTR_DRAGGABLE, false);
}

/*
 * Initializes the color picker
 */
function initColorPicker() {

    $('#color-picker').farbtastic('#color-value');
}

/*
 * Refreshes weather information 
 * and engages markup elements to take values
 * 
 * @param unit - can be 'c' for metric or 'f' for royal
 */
function displayWeatherInfo(unit) {
    getWeatherInfo(unit);
    
    $('#outside-location').fadeOut(20).html(WEATHER_OUTSIDE_LOCATION).fadeIn(120);
    $('#outside-condition').fadeOut(20).html(WEATHER_OUTSIDE_CONDITION).fadeIn(220);
    $('#outside-temperature').fadeOut(20).html(WEATHER_OUTSIDE_TEMPERATURE).fadeIn(320);
    $('#outside-chill').fadeOut(20).html(WEATHER_OUTSIDE_WIND_CHILL).fadeIn(420);
    $('#outside-wind').fadeOut(20).html(WEATHER_OUTSIDE_WIND_SPEED).fadeIn(520);
    $('#outside-humidity').fadeOut(20).html(WEATHER_OUTSIDE_HUMIDITY).fadeIn(620);
    $('#outside-pressure').fadeOut(20).html(WEATHER_OUTSIDE_PRESSURE).fadeIn(720);
    $('#outside-visibility').fadeOut(20).html(WEATHER_OUTSIDE_VISIBILITY).fadeIn(820);
    $('#outside-sunrise').fadeOut(20).html(WEATHER_OUTSIDE_SUNRISE).fadeIn(920);
    $('#outside-sunset').fadeOut(20).html(WEATHER_OUTSIDE_SUNSET).fadeIn(1020);
}

/*
 * Monitors change in temp format selector
 */
function initTempFormatState() {
    $('#radio1').change(function(){
        if ($(this).attr('checked', 'checked')) {
            displayWeatherInfo('c');
        }
    });
    
    $('#radio2').change(function(){
        if ($(this).attr('checked', 'checked')) {
            var tempHumidityValue = $('#current-temp').html();
            if (tempHumidityValue !== '-') {
                var dataParts = $('#current-temp').html().split('/');
                var formattedTemp = '-';
                if (dataParts.length > 0) {
                    var temp = parseFloat(dataParts[0]);
                    formattedTemp = Math.floor(((temp * (9 / 5)) + 32)) + '&deg;&nbsp;/&nbsp;' + dataParts[1];
                    $('#current-temp').html(formattedTemp);
                }
            } else {
                $('#current-temp').html('-');
            }
            
            displayWeatherInfo('f');
        }
    });
}

/*
 * Initializes the button state for the Settings section
 */
function initSettingsButtonState() {
    $('#settings-data').find('input:checkbox[id!=toggle-9]').click(function() {
        //if the checkbox is not checked, check it and uncheck 
        //all other checkboxes besides Arduino reset
        if (! $(this).attr('checked')) {
            $(this).attr('checked', 'checked');
            var checkedId = $(this).attr('id');
            $(this).parent().find('input:checkbox').each(function() {
                if ($(this).attr('id') !== checkedId && $(this).attr('id') !== 'toggle-9') {
                    $(this).removeAttr('checked');
                }
            });
            
            if (checkedId === 'toggle-4') { //everything-off button
                $('#motion-data *').removeAttr('disabled').on('click');
                $('#motion-data').css('opacity', '1');
                $('#led-data *').removeAttr('disabled').on('click');
                $('#led-data').css('opacity', '1');
                $('#temp-data *').removeAttr('disabled').on('click');
                $('#temp-data').css('opacity', '1');
            }
        //otherwise, uncheck the checkbox
        } else {
            $(this).removeAttr('checked');
        }
    });
}

/*
 * Initializes the button state for the Motion sensor section at first
 */
function firstInitMotionSensorState() {
    var params = {
        module: 'device',
        unit: 'service',
        method: 'getDeviceState',
        token: token,
        params: {
            device: 'motion'
        }
    };
    
    $.ajax({
        type: 'POST',
        url: AJAX_URL,
        data: 'params=' + JSON.stringify(params),
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        cache: false,
        dataType: 'json'
    }).success(function(data) {
        if(data) {
            if (data.additional_data === 'trigger_led:out_home' || data.additional_data === 'out_home') {
                $('#toggle-5').trigger('click');
            }
        }
    });
}

/*
 * Re-initializes the button state for the Motion sensor section repeatedly
 */
function initMotionSensorState() {
    var params = {
        module: 'device',
        unit: 'service',
        method: 'getDeviceState',
        token: token,
        params: {
            device: 'motion'
        }
    };
    
    $.ajax({
        type: 'POST',
        url: AJAX_URL,
        data: 'params=' + JSON.stringify(params),
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        cache: false,
        dataType: 'json'
    }).success(function(data) {
        if(data) {
            if (data.last_updated) {
                if (data.state === 'ON' && data.last_updated !== $('#motion-trigger-time').html()) {
                    $('#motion-trigger-time').addClass("time-change-event");
                    setTimeout(function(){
                        $('#motion-trigger-time').removeClass("time-change-event");
                    },500);
                }
                
                $('#motion-trigger-time').html(data.last_updated);
            }
            
            if (data.state === 'ON') {
                $('#toggle-1').attr('checked', 'checked');
            } else {
                $('#toggle-1').removeAttr('checked');
                $('#motion-trigger-time').html('-');
            }
            
            if (data.additional_data === 'trigger_led:out_home' || data.additional_data === 'out_home') {
                 $('#motion-data *').removeAttr('disabled').on('click');
                 $('#motion-data').css('opacity', '1');
            }
            
            if (data.state === 'ON' && (data.additional_data === 'trigger_led:out_home' || data.additional_data === 'trigger_led')) {
                $('#toggle-2').attr('checked', 'checked');
                
                var lastUpdateTime = formatDate(data.last_updated);
                
                var innerParams = {
                    module: 'device',
                    unit: 'service',
                    method: 'getDeviceState',
                    token: token,
                    params: {
                        device: 'led'
                    }
                };
                 $.ajax({
                    type: 'POST',
                    url: AJAX_URL,
                    data: 'params=' + JSON.stringify(innerParams),
                    headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                    cache: false,
                    dataType: 'json'
                }).success(function(ledData) {
                    if (ledData.state === '(0,0,0)!') {
                        if (lastUpdateTime + 5000 > Date.now()) {
                            console.log("Blinking...");
                        }
                    }
                });
            } else {
                $('#toggle-2').removeAttr('checked');
            }
        }
    });
}
    
/*
 * Initializes the button state of the LED Lamp section
 */
function initLEDLampState() {
    var params = {
        module: 'device',
        unit: 'service',
        method: 'getDeviceState',
        token: token,
        params: {
            device: 'led'
        }
    };
    
    $.ajax({
        type: 'POST',
        url: AJAX_URL,
        data: 'params=' + JSON.stringify(params),
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        cache: false,
        dataType: 'json'
    }).success(function(data) {
        if(data) {
            if (data.state === '(0,0,0)!') {
                $('#toggle-3').removeAttr('checked');
                $('#current-color').css({'background-color': 'transparent', 'border' : '1px solid #792D5C'});
                $('#color-picker').css('opacity', '0.4');
            } else {
                $('#toggle-3').attr('checked', 'checked');
                $('#current-color').css({'background-color': 'rgb' + data.state.split('!')[0], 'border': 'none'});
                $('#color-picker').css('opacity', '1');
            }
            
            if (typeof data.additional_data !== 'undefined') {
                switch(data.additional_data) {
                    case 'all_off':
                        //If one device has all_off, then all devices are off
                        $('#toggle-4').trigger('click');
                        break;
                    case 'mode_ambient':
                        $('#toggle-6').trigger('click');
                        break;
                    case 'mode_relax':
                        $('#toggle-7').trigger('click');
                        break;
                    case 'mode_movie':
                        $('#toggle-8').trigger('click');
                        break;
                }
            }
        }
    });
}

/*
 * Initializes the temp & humidity sensor
 */
function initTempSensorState() {
    var params = {
        module: 'device',
        unit: 'service',
        method: 'getDeviceState',
        token: token,
        params: {
            device: 'temp'
        }
    };
    
    $.ajax({
        type: 'POST',
        url: AJAX_URL,
        data: 'params=' + JSON.stringify(params),
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        cache: false,
        dataType: 'json'
    }).success(function(data) {
        if(data && data.state !== '-') {
            var dataParts = data.state.split('/');
            var temp = parseFloat(dataParts[0]);
            var formattedTemp;
            if ($('#radio1').is(':checked')) {
                formattedTemp = temp;
                formattedTemp += '&deg;&nbsp;/&nbsp;' + dataParts[1] + '%';
                $('#current-temp').html(formattedTemp);
            }
        } else {
            $('#current-temp').html('-');
        }
    });
}

//Creates a JavaScript date from a string in format yyyy-mm-dd hh-mm-ss
function formatDate(humanReadableDate) {
    var humanDateParts = humanReadableDate.split(' ');
    var date = humanDateParts[0];
    var time = humanDateParts[1];
    
    var dateParts = date.split('-');
    var timeParts = time.split(':');
    
    var year = dateParts[0];
    var month = dateParts[1];
    var day = dateParts[2];
    
    var hours = timeParts[0];
    var minutes = timeParts[1];
    var seconds = timeParts[2];
    
    return new Date(year, month - 1, day, hours, minutes, seconds).getTime();
}