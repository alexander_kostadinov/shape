"use strict";

//pseudo constants
var HTML_TAG_SELECTOR = 'html';
var BODY_TAG_SELECTOR = 'body';
var IMG_TAG_SELECTOR = 'img';
var GO_TO_TOP_BTN_SELECTOR = '#go-top';
var GO_TO_TOP_BTN_TXT = 'Back to top';
var ATTR_DRAGGABLE = 'draggable';
var ENTITY_UP_ARROW = '&uarr;';
var AJAX_URL = '/shape/init.php';
var UNDEFINED = 'undefined';
var YAHOO_WEATHER_ENDPOINT_METRIC = 'https://query.yahooapis.com/v1/public/yql' + 
                        '?format=json&q=select * from weather.forecast' + 
                        ' where woeid in (select woeid from geo.places(1)' + 
                        ' where text="sofia, bg") and u="c"';
var YAHOO_WEATHER_ENDPOINT_ROYAL = 'https://query.yahooapis.com/v1/public/yql' + 
                        '?format=json&q=select * from weather.forecast' + 
                        ' where woeid in (select woeid from geo.places(1)' + 
                        ' where text="sofia, bg") and u="f"';
                     
//placeholder variables
var WEATHER_OUTSIDE_LOCATION = '';
var WEATHER_OUTSIDE_WIND_SPEED = '';
var WEATHER_OUTSIDE_WIND_CHILL = '';
var WEATHER_OUTSIDE_HUMIDITY = '';
var WEATHER_OUTSIDE_PRESSURE = '';
var WEATHER_OUTSIDE_VISIBILITY = '';
var WEATHER_OUTSIDE_SUNRISE = '';
var WEATHER_OUTSIDE_SUNSET = '';
var WEATHER_OUTSIDE_TEMPERATURE = '';
var WEATHER_OUTSIDE_CONDITION = '';

//idle time placeholder variable
var IDLE_TIME;