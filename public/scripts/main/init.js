"use strict";

/*
 * Calls the necessary methods on page load 
 */
$(document).ready(function() {
    initMarkupScrolling();
    initBackToTopButton();
    makeElementsFluid();
    getUserInfo();
    initColorPicker();
    displayWeatherInfo('c');
    initSettingsButtonState();
    firstInitMotionSensorState();
    initLEDLampState();
    
    window.setInterval(function(){
        initMotionSensorState();
        initTempSensorState();
    }, 5000);
    
    window.setInterval(function(){
        var format = '';
        
        $('body').find('.radio:checked').each(function(format){
            if ($(this).attr('id') == 'radio1' || $(this).attr('id') == 'radio2') {
                format = $(this).val();
                displayWeatherInfo(format);
            }
        });
        
    }, 60000);
    
    initTempFormatState();
    
    $('#username-holder').click(function() {
        logOut();
    });
    
    initInactivityCheck();
});

/*
 * Gets information about the currently logged-in user
 */
function getUserInfo() {
    var params = {
        module: 'login',
        unit: 'service',
        method: 'readUserInfo',
        token: token,
        params: {}
    };
    
    $.ajax({
        type: 'POST',
        url: AJAX_URL,
        data: 'params=' + JSON.stringify(params),
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        cache: false,
        dataType: 'json'
    }).success(function(data) {
        var tooltipText;
        
        if (typeof data.username !== UNDEFINED 
            && typeof data.last_login !== UNDEFINED 
            && typeof data.ip !== UNDEFINED) {
        
            tooltipText = 'Last login:\n' + data.last_login + '\nIP: ' + data.ip + '\nClick to logout.';
        } else {
            tooltipText = 'No user info.\nClick to logout.'
        }
        
        var displWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        
        var tooltipPlacement = 'left';
        
        if (displWidth <= 360) {
            tooltipPlacement = 'bottom';
        }
        
        $('#username-holder').html('Hi, ' + data.username + '<span id="logout-span" class="glyphicon glyphicon-off"></span>')
                                 .attr('data-original-title', tooltipText)
                                 .attr('data-placement', tooltipPlacement).tooltip();
    });
}

/*
 * Logs the user out
 */
function logOut() {
    var params = {
        module: 'login',
        unit: 'service',
        method: 'doLogout',
        token: token,
        params: {}
    };
    
    $.ajax({
        type: 'POST',
        url: AJAX_URL,
        data: 'params=' + JSON.stringify(params),
        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
        cache: false,
        dataType: 'json'
    }).success(function(data) {
        if (data.hasLoggedOut !== UNDEFINED && data.hasLoggedOut == true) {
            window.location.href = 'login';
        }
    });
}

/*
 * Gets current weather info for Sofia, BG
 * from Yahoo Weather Service
 * 
 * @param unit - can be 'c' for metric or 'f' for royal
 */
function getWeatherInfo(unit) {
    if (typeof unit !== UNDEFINED) {
        //reset placeholder variables
        WEATHER_OUTSIDE_LOCATION = 'Location: ';
        WEATHER_OUTSIDE_WIND_SPEED = 'Wind speed: ';
        WEATHER_OUTSIDE_WIND_CHILL = 'Feels like: ';
        WEATHER_OUTSIDE_HUMIDITY = 'Humidity: ';
        WEATHER_OUTSIDE_PRESSURE = 'Pressure: ';
        WEATHER_OUTSIDE_VISIBILITY = 'Visibility: ';
        WEATHER_OUTSIDE_SUNRISE = 'Sunrise: ';
        WEATHER_OUTSIDE_SUNSET = 'Sunset: ';
        WEATHER_OUTSIDE_TEMPERATURE = 'Current outdoor temperature: ';
        WEATHER_OUTSIDE_CONDITION = 'Condition: ';

        var weatherAjaxUrl = '';
        
        switch(unit) {
            case 'c':
                weatherAjaxUrl = YAHOO_WEATHER_ENDPOINT_METRIC;
                break;
            case 'f':
                weatherAjaxUrl = YAHOO_WEATHER_ENDPOINT_ROYAL;
                break;
            default:
                weatherAjaxUrl = YAHOO_WEATHER_ENDPOINT_METRIC;
                break;
        }
        
        $.ajax({
            type: 'GET',
            url: weatherAjaxUrl,
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false,
            dataType: 'json',
            async: false
        }).success(function(data) {
            if (data && data.query.results.channel !== UNDEFINED) {
                var weather = data.query.results.channel;

                WEATHER_OUTSIDE_LOCATION += 
                        typeof weather.location !== UNDEFINED ? 
                        weather.location.city + ', ' +  weather.location.country : '-';
                WEATHER_OUTSIDE_WIND_SPEED += 
                        typeof weather.wind.speed !== UNDEFINED ? 
                        weather.wind.speed + ' ' + weather.units.speed : '-';
                WEATHER_OUTSIDE_WIND_CHILL += 
                        typeof weather.wind.chill !== UNDEFINED ? 
                        weather.wind.chill + ' ' + weather.units.temperature : '-';
                WEATHER_OUTSIDE_HUMIDITY += 
                        typeof weather.atmosphere.humidity !== UNDEFINED ? 
                        weather.atmosphere.humidity + ' %' : '-';
                WEATHER_OUTSIDE_PRESSURE += 
                        typeof weather.atmosphere.pressure !== UNDEFINED ? 
                        weather.atmosphere.pressure + ' ' + weather.units.pressure : '-';
                WEATHER_OUTSIDE_VISIBILITY += 
                        typeof weather.atmosphere.visibility !== UNDEFINED ? 
                        weather.atmosphere.visibility + ' ' + weather.units.distance : '-';
                WEATHER_OUTSIDE_SUNRISE += 
                        typeof weather.astronomy.sunrise !== UNDEFINED ? 
                        weather.astronomy.sunrise : '-';
                WEATHER_OUTSIDE_SUNSET += 
                        typeof weather.astronomy.sunset !== UNDEFINED ? 
                        weather.astronomy.sunset : '-';
                WEATHER_OUTSIDE_TEMPERATURE += 
                        typeof weather.item.condition.temp !== UNDEFINED ? 
                        weather.item.condition.temp + ' ' + weather.units.temperature : '-';
                WEATHER_OUTSIDE_CONDITION += 
                        typeof weather.item.condition.text !== UNDEFINED ? 
                        weather.item.condition.text : '-';
            }
        });
    }
}

/*
 * Initializes the user inactivity check
 */
function initInactivityCheck() {
    IDLE_TIME = 0;
    
    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement or key press.
    $('body').mousemove(function (e) {
        IDLE_TIME = 0;
    });
    
    $('body').keypress(function (e) {
        IDLE_TIME = 0;
    });
}
   
/*
 * Idle timer incrementing function
 */
function timerIncrement() {
    IDLE_TIME = IDLE_TIME + 1;
    
    if (IDLE_TIME > 14) { // 15 minutes
        logOut();
    }
}
