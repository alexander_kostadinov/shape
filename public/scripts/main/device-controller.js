"use strict";

$(document).ready(function() {
    //motion sensor ON click
    $('#toggle-1').change(function(){
        var state = 'OFF';
        if ($(this).is(':checked')) {
            state = 'ON';
        }
        
        var params = {
            module: 'device',
            unit: 'service',
            method: 'updateDeviceState',
            token: token,
            params: {
                device: 'motion',
                state: state
            }
        };
        
        $.ajax({
            type: 'POST',
            url: AJAX_URL,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false,
            dataType: 'json'
        }).success(function(data) {
            if(data) {
                if (data.last_updated) {
                    $('#motion-trigger-time').html(data.last_updated);
                }
                
                if (data.state === 'ON') {
                    $('#toggle-1').attr('checked', 'checked');
                } else {
                    $('#toggle-1').removeAttr('checked');
                    $('#motion-trigger-time').html('-');
                }
            }
        });
    });
    
    //motion sensor TRIGGER LED click
    $('#toggle-2').change(function(){
        var additional = '-';
        if ($(this).is(':checked')) {
            additional = 'trigger_led';
        }
        
        var params = {
            module: 'device',
            unit: 'service',
            method: 'addDeviceParams',
            token: token,
            params: {
                device: 'motion',
                additional: additional
            }
        };
        
        $.ajax({
            type: 'POST',
            url: AJAX_URL,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false,
            dataType: 'json'
        }).success(function(data) {
            if(data && data.additional_params) {
                if (data.additional_data === 'trigger_led:out_home' || data.additional_data === 'trigger_led') {
                    $('#toggle-2').attr('checked', 'checked');
                } else {
                    $('#toggle-2').removeAttr('checked');
                }
            }
        });
    });
    
    //LED LAMP on/off button
    $('#toggle-3').change(function(){
        var state = '(0,0,0)';
        
        if ($(this).is(':checked')) {
            state = $('#color-value').css('background-color').split('rgb')[1].replace(/\s/g,'');
        }
        
        var params = {
            module: 'device',
            unit: 'service',
            method: 'updateDeviceState',
            token: token,
            params: {
                device: 'led',
                state: state
            }
        };
        
        $.ajax({
            type: 'POST',
            url: AJAX_URL,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false,
            dataType: 'json'
        }).success(function(data) {
            if(data) {
                if (data.state === '(0,0,0)!') {
                    $('#toggle-3').removeAttr('checked');
                    $('#current-color').css({'background-color': 'transparent', 'border' : '1px solid #792D5C'});
                    $('#color-picker').css('opacity', '0.4');
                } else {
                    $('#toggle-3').attr('checked', 'checked');
                    $('#current-color').css({'background-color': 'rgb' + data.state.split('!')[0], 'border': 'none'});
                    $('#color-picker').css('opacity', '1');
                }
                
                insertAdditionalDeviceInfo('led', '-');
                initLEDLampState();
            }
        });
    });
    
    //LED LAMP color picker when state is ON
    $('.sl-marker').mouseup(function(){
        var state = '(0,0,0)';
        
        if ($('#toggle-3').is(':checked')) {
            state = $('#color-value').css('background-color').split('rgb')[1].replace(/\s/g,'');
        }
        
        var params = {
            module: 'device',
            unit: 'service',
            method: 'updateDeviceState',
            token: token,
            params: {
                device: 'led',
                state: state
            }
        };
        
        $.ajax({
            type: 'POST',
            url: AJAX_URL,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false,
            dataType: 'json'
        }).success(function(data) {
            if(data) {
                if (data.state === '(0,0,0)!') {
                    $('#toggle-3').removeAttr('checked');
                    $('#current-color').css({'background-color': 'transparent', 'border' : '1px solid #792D5C'});
                } else {
                    $('#toggle-3').attr('checked', 'checked');
                    $('#current-color').css({'background-color': 'rgb' + data.state.split('!')[0], 'border': 'none'});
                }
                
                insertAdditionalDeviceInfo('led', '-');
            }
        });
    });
    
    //everything OFF pushed
    $('#toggle-4').on('change', function(){
        if ($('#toggle-4').is(':checked')) {
            var params = {
                module: 'device',
                unit: 'service',
                method: 'updateDeviceState',
                token: token,
                params: {
                    device: 'motion',
                    state: 'OFF'
                }
            };

            $.ajax({
                type: 'POST',
                url: AJAX_URL,
                data: 'params=' + JSON.stringify(params),
                headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                cache: false,
                dataType: 'json'
            }).success(function(data) {
                if(data) {
                    var params = {
                        module: 'device',
                        unit: 'service',
                        method: 'updateDeviceState',
                        token: token,
                        params: {
                            device: 'led',
                            state: '(0,0,0)'
                        }
                    };

                    $.ajax({
                        type: 'POST',
                        url: AJAX_URL,
                        data: 'params=' + JSON.stringify(params),
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                        cache: false,
                        dataType: 'json'
                    }).success(function(data) {
                        if(data) {
                            var params = {
                                module: 'device',
                                unit: 'service',
                                method: 'updateDeviceState',
                                token: token,
                                params: {
                                    device: 'temp',
                                    state: '-'
                                }
                            };

                            $.ajax({
                                type: 'POST',
                                url: AJAX_URL,
                                data: 'params=' + JSON.stringify(params),
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                                cache: false,
                                dataType: 'json'
                            }).success(function(data) {
                                if(data) {
                                    //Insert all-off to one device's additional info field
                                    //so if one device has 'all-off', then all devices are off
                                    insertAdditionalDeviceInfo('led', 'all_off');
                                    $('#motion-data *').attr('disabled', 'disabled').off('click');
                                    $('#motion-data').css('opacity', '0.3');
                                    $('#led-data *').attr('disabled', 'disabled').off('click');
                                    $('#led-data').css('opacity', '0.3');
                                    $('#temp-data *').attr('disabled', 'disabled').off('click');
                                    $('#temp-data').css('opacity', '0.3');
                                    $('#toggle-4').nextUntil('label[for=toggle-8]', '.toggle-button-label').css('opacity', '0.3');
                                    $('#toggle-4').nextUntil('#toggle-9', '.btn-toggle').attr('disabled', 'disabled');
                                    $('#current-color').css({'background-color': 'rgb' + data.state.split('!')[0], 'border': 'none'});
                                    $('#toggle-3').removeAttr('checked');
                                }
                            });
                        }
                    });
                }
            });
        } else {
            insertAdditionalDeviceInfo('motion', $('#toggle-2').is(':checked') ? 'trigger_led' : '-'); //TODO implement for temp as well!
            insertAdditionalDeviceInfo('led', '-');
            insertAdditionalDeviceInfo('temp', '-');
            $('#motion-data *').removeAttr('disabled').on('click');
            $('#motion-data').css('opacity', '1');
            $('#led-data *').removeAttr('disabled').on('click');
            $('#led-data').css('opacity', '1');
            $('#temp-data *').removeAttr('disabled').on('click');
            $('#temp-data').css('opacity', '1');
            $('#toggle-4').nextUntil('label[for=toggle-8]', '.toggle-button-label').css('opacity', '1');
            $('#toggle-4').nextUntil('#toggle-9', '.btn-toggle').removeAttr('disabled');
            window.setTimeout(function(){
                initLEDLampState();
            }, 1000);
            
        }
    });
    
    
    //Out-of-home mode clicked
    $('#toggle-5').change(function(){
        if ($('#toggle-5').is(':checked')) {
            var params = {
                module: 'device',
                unit: 'service',
                method: 'updateDeviceState',
                token: token,
                params: {
                    device: 'motion',
                    state: 'ON'
                }
            };

            $.ajax({
                type: 'POST',
                url: AJAX_URL,
                data: 'params=' + JSON.stringify(params),
                headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                cache: false,
                dataType: 'json'
            }).success(function(data) {
                if(data) {
                    var additional;
                    additional = $('#toggle-2').is(':checked') ? 'trigger_led:out_home' : 'out_home';
                    insertAdditionalDeviceInfo('motion', additional);
                    
                    var params = {
                        module: 'device',
                        unit: 'service',
                        method: 'updateDeviceState',
                        token: token,
                        params: {
                            device: 'led',
                            state: '(0,0,0)'
                        }
                    };

                    $.ajax({
                        type: 'POST',
                        url: AJAX_URL,
                        data: 'params=' + JSON.stringify(params),
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                        cache: false,
                        dataType: 'json'
                    }).success(function(data) {
                        if(data) {
                            $('#current-color').css({'background-color': 'transparent', 'border' : '1px solid #792D5C'});
                            var params = {
                                module: 'device',
                                unit: 'service',
                                method: 'updateDeviceState',
                                token: token,
                                params: {
                                    device: 'temp',
                                    state: '-'
                                }
                            };

                            $.ajax({
                                type: 'POST',
                                url: AJAX_URL,
                                data: 'params=' + JSON.stringify(params),
                                headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                                cache: false,
                                dataType: 'json'
                            }).success(function(data) {
                                if(data) {
                                    insertAdditionalDeviceInfo('led', '-');
                                    insertAdditionalDeviceInfo('temp', '-');
                                    $('#led-data *').attr('disabled', 'disabled').off('click');
                                    $('#led-data').css('opacity', '0.3');
                                    $('#temp-data *').attr('disabled', 'disabled').off('click');
                                    $('#temp-data').css('opacity', '0.3');
                                    $('#toggle-3').removeAttr('checked');
                                }
                            });
                        }
                    });
                }
            });
        } else {
            $('#led-data *').removeAttr('disabled').on('click');
            $('#led-data').css('opacity', '1');
            $('#toggle-3').trigger('click');
            $('#temp-data *').removeAttr('disabled').on('click');
            $('#temp-data').css('opacity', '1');
            var additional;
            additional = $('#toggle-2').is(':checked') ? 'trigger_led' : '-';
            insertAdditionalDeviceInfo('motion', additional);
        }
    });
    
    //Ambient mode clicked
    $('#toggle-6').change(function(){
        if ($('#toggle-6').is(':checked')) {
             var params = {
                module: 'device',
                unit: 'service',
                method: 'updateDeviceState',
                token: token,
                params: {
                    device: 'led',
                    predefined: 'true',
                    state: 'ambient'
                }
            };

            $.ajax({
                type: 'POST',
                url: AJAX_URL,
                data: 'params=' + JSON.stringify(params),
                headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                cache: false,
                dataType: 'json'
            }).success(function(data) {
                if( data) {
                    $('#led-data *').attr('disabled', 'disabled').off('click');
                    $('#led-data').css('opacity', '0.3');
                    insertAdditionalDeviceInfo('led', 'mode_ambient');
                    insertAdditionalDeviceInfo('motion', '-');
                    $('#toggle-3').attr('checked', 'checked');
                    $('#current-color').css({'background-color': 'rgb' + data.state.split('!')[0], 'border': 'none'});
                    $('#motion-data *').removeAttr('disabled').on('click');
                    $('#motion-data').css('opacity', '1');
                    $('#temp-data *').removeAttr('disabled').on('click');
                    $('#temp-data').css('opacity', '1');
                }
            });
        } else {
            $('#led-data *').removeAttr('disabled').on('click');
            $('#led-data').css('opacity', '1');
            $('#toggle-3').trigger('click');
        }
    });
    
    //Relax mode clicked
    $('#toggle-7').change(function(){
        if ($('#toggle-7').is(':checked')) {
             var params = {
                module: 'device',
                unit: 'service',
                method: 'updateDeviceState',
                token: token,
                params: {
                    device: 'led',
                    predefined: 'true',
                    state: 'relax'
                }
            };

            $.ajax({
                type: 'POST',
                url: AJAX_URL,
                data: 'params=' + JSON.stringify(params),
                headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                cache: false,
                dataType: 'json'
            }).success(function(data) {
                if (data) {
                    $('#led-data *').attr('disabled', 'disabled').off('click');
                    $('#led-data').css('opacity', '0.3');
                    insertAdditionalDeviceInfo('led', 'mode_relax');
                    insertAdditionalDeviceInfo('motion', '-');
                    $('#toggle-3').attr('checked', 'checked');
                    $('#current-color').css({'background-color': 'rgb' + data.state.split('!')[0], 'border': 'none'});
                    $('#motion-data *').removeAttr('disabled').on('click');
                    $('#motion-data').css('opacity', '1');
                    $('#temp-data *').removeAttr('disabled').on('click');
                    $('#temp-data').css('opacity', '1');
                }
            });
        } else {
            $('#led-data *').removeAttr('disabled').on('click');
            $('#led-data').css('opacity', '1');
            $('#toggle-3').trigger('click');
        }
    });
    
    //Movie mode clicked
    $('#toggle-8').change(function(){
        if ($('#toggle-8').is(':checked')) {
             var params = {
                module: 'device',
                unit: 'service',
                method: 'updateDeviceState',
                token: token,
                params: {
                    device: 'led',
                    predefined: 'true',
                    state: 'movie'
                }
            };

            $.ajax({
                type: 'POST',
                url: AJAX_URL,
                data: 'params=' + JSON.stringify(params),
                headers : {'Content-Type': 'application/x-www-form-urlencoded'},
                cache: false,
                dataType: 'json'
            }).success(function(data) {
                if (data) {
                    $('#led-data *').attr('disabled', 'disabled').off('click');
                    $('#led-data').css('opacity', '0.3');
                    insertAdditionalDeviceInfo('led', 'mode_movie');
                    insertAdditionalDeviceInfo('motion', '-');
                    $('#toggle-3').attr('checked', 'checked');
                    $('#current-color').css({'background-color': 'rgb' + data.state.split('!')[0], 'border': 'none'});
                    $('#motion-data *').removeAttr('disabled').on('click');
                    $('#motion-data').css('opacity', '1');
                    $('#temp-data *').removeAttr('disabled').on('click');
                    $('#temp-data').css('opacity', '1');
                }
            });
        } else {
            $('#led-data *').removeAttr('disabled').on('click');
            $('#led-data').css('opacity', '1');
            $('#toggle-3').trigger('click');
        }
    });
    
    /*
    * Inserts additional device data
    */
    function insertAdditionalDeviceInfo(device, info) {
        if (typeof device === 'undefined' || typeof info === 'undefined') {
            
            return;
        }
        var params = {
            module: 'device',
            unit: 'service',
            method: 'addDeviceParams',
            token: token,
            params: {
                device: device,
                additional: info
            }
        };
        
        $.ajax({
            type: 'POST',
            url: AJAX_URL,
            data: 'params=' + JSON.stringify(params),
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            cache: false,
            dataType: 'json'
        }).success(function(data) {
           //No actions to be performed here
        });
    }
});