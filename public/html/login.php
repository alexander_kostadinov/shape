<?php
define('IS_VALID', true);
include '../../init.php';
if (isset($_SESSION['username'])) {
    header('Location: index');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../img/favicon.png">
        <title>SHAPE</title>
        
        <link rel="stylesheet" href="../lib/bootstrap-3.3.5/css/bootstrap.css" />
        <link rel="stylesheet" href="../lib/bootstrap-3.3.5/css/bootstrap-theme.css" />
        <link rel="stylesheet" href="../styles/main_login.css" />
        
        <script type="text/javascript" src="../lib/jQuery-2.1.4.js"></script>
        <script type="text/javascript" src="../lib/bootstrap-3.3.5/js/bootstrap.js"></script>
        <script type="text/javascript" src="../scripts/common/processor.js"></script>
        <script type="text/javascript" src="../scripts/login/config.js"></script>
        <script type="text/javascript" src="../scripts/login/init.js"></script>
    </head>
    
    <body>
        <div id="header">
            <p id="header-title">shape</p><img id="logo" src="../img/icon-main.png" />
        </div>
        <div class="container-fluid">
            <div id="form-container" class="row">
                <h1>Enter Smart Home</h1>
                <form>
					<div class="form-group">
					    <div class="input-group" id="username-group">
						    <span class="input-group-addon">
						        <span class="glyphicon glyphicon-user "></span>
						    </span>
						    <input type="text" class="form-control input-lg" id="username" placeholder="Username" />
					    </div>
					</div>
					<div class="form-group">
	                    <div class="input-group" id="password-group">
	                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-lock"></span>
                            </span>
		                   <input type="password" class="form-control input-lg" id="password" placeholder="Password" />
	                    </div>
                    </div>
					<button id="btn-login" type="submit" class="btn btn-lg">
					   <span class="glyphicon glyphicon-log-in"></span>
					   Login
				   </button>
				</form>
            </div>
        </div>
        <div id="footer">
            <p id="signature"><span class="letter-highlight">S</span>mart <span class="letter-highlight">H</span>ome <span class="letter-highlight">A</span>rduino <span class="letter-highlight">P</span>latfotm & <span class="letter-highlight">E</span>ngine</p>
        </div>
    </body>
</html>
