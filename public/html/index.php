<?php
define('IS_VALID', true);
include '../../init.php';
if (! isset($_SESSION['username'])) {
    header('Location: login');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="icon" href="../img/favicon.png">
	    <title>SHAPE</title>
	    
	    <link rel="stylesheet" href="../lib/bootstrap-3.3.5/css/bootstrap.css" />
        <link rel="stylesheet" href="../lib/bootstrap-3.3.5/css/bootstrap-theme.css" />
        <link rel="stylesheet" type="text/css" href="../lib/farbtastic/farbtastic.css"></link>
	    <link rel="stylesheet" href="../styles/main.css" />
	    
	    <script type="text/javascript" src="../lib/jQuery-2.1.4.js"></script>
	    <script type="text/javascript" src="../lib/bootstrap-3.3.5/js/bootstrap.js"></script>
	    <script type="text/javascript" src="../lib/farbtastic/farbtastic.js"></script>
	    <script type="text/javascript" src="../scripts/common/processor.js"></script>
	    <script type="text/javascript" src="../scripts/main/config.js"></script>
	    <script type="text/javascript" src="../scripts/main/controls-rendering.js"></script>
	    <script type="text/javascript" src="../scripts/main/init.js"></script>
	    <script type="text/javascript" src="../scripts/main/device-controller.js"></script>
	</head>
	
	<body>
        <div id="main-container">
	        <div id="header">
	            <p id="header-title">shape</p>
	            <img id="logo" src="../img/icon-main.png" />
	            <p id="username-holder"></p>
	        </div>
	        <h1>Home Controls</h1>
	        
	        <a href="#" id="go-top" style="display: none;">Back to top</a>
	        
	        <div id="controls-container">
	            <a href="#motion-sensor" class="function-link"><div id="icon-motion-sensor" class="icon-div"></div>Motion sensor</a>
	            <a href="#led-lamp" class="function-link"><div id="icon-led-lamp" class="icon-div"></div>LED Lamp</a>
	            <a href="#temp-sensor" class="function-link"><div id="icon-temp-sensor" class="icon-div"></div>Temperature</a>
	            <a href="#settings" class="function-link"><div id="icon-settings" class="icon-div"></div>Settings</a>
	        </div>
	        
	        <img class="divider-image" src="../img/divider.png" />
	        <h2 class="controls-content-title" id="motion-sensor" name="motion-sensor">Motion Sensor</h2>
	        <div class="controls-content"  id="motion-data">
	            <img class="controls-content-img" src="../img/motion-modal-inside.png" />
	            <label class="toggle-button-label">Turn on/off the motion sensor</label>
	            <br />
	            <input type="checkbox" name="toggle-1" id="toggle-1" class="btn-toggle">
	            <label for="toggle-1"></label>
	            <label class="toggle-button-label">Triger <a href="#led-lamp">LED lamp</a> when ON</label>
	            <br />
	            <input type="checkbox" name="toggle-2" id="toggle-2" class="btn-toggle">
	            <label for="toggle-2"></label>
	            <label class="toggle-button-label">Last triggered time: <span id="motion-trigger-time">-</span></label>
	        </div>
	        
	        <img class="divider-image" src="../img/divider.png" />
	        <h2 class="controls-content-title" id="led-lamp" name="led-lamp">LED Lamp</h2>
	        <div class="controls-content" id="led-data">
	            <img class="controls-content-img" src="../img/led-modal-inside.png" />
	            <label class="toggle-button-label">Turn on/off the LED lamp</label>
	            <div id="current-color"></div>
	            <input type="checkbox" name="toggle-3" id="toggle-3" class="btn-toggle">
	            <label for="toggle-3"></label>
	            <label class="toggle-button-label" id="color-picker-label">Select lamp color</label>
	            <input type="hidden" id="color-value" value="#000000" />
	            <div id="color-picker"></div>
	        </div>
	        
	        <img class="divider-image" src="../img/divider.png" />
	        <h2 class="controls-content-title" id="temp-sensor" name="temp-sensor">Temp. & Humidity</h2>
            <div class="controls-content" id="temp-data">
                <img class="controls-content-img" src="../img/temp-modal-inside.png" />
                <label class="toggle-button-label">Current indoor temperature/humidity: <span id="current-temp"></span></label>
                <br /><br /><br />
                <div id="temp-controls-container">
	                <div>
		                <input type="radio" name="radio-temp" id="radio1" class="radio" value="c" checked="checked"/>
		                <label class="btn-radio" for="radio1">Celsius</label>
	                </div>
                    <img class="temp-unit-img" src="../img/icon-celsius.png" />
	                <br /><br /><br />
	                <div>
		                <input type="radio" name="radio-temp" id="radio2" class="radio" value="f"/>
		                <label class="btn-radio" for="radio2">Farenheit</label>
	                </div>
                    <img class="temp-unit-img" src="../img/icon-farenheit.png" />
	                <label class="toggle-button-label temp-value" id="outside-location"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-condition"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-temperature"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-chill"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-wind"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-humidity"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-pressure"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-visibility"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-sunrise"></label><br />
	                <label class="toggle-button-label temp-value" id="outside-sunset"></label><br />
                </div>
            </div>
            
            <img class="divider-image" src="../img/divider.png" />
            <h2 class="controls-content-title" id="settings" name="settings">General Settings</h2>
            <div class="controls-content" id="settings-data">
                <img class="controls-content-img" src="../img/settings-modal-inside.png" />
                <label class="toggle-button-label">Everything-off mode</label>
                    <br />
                    <input type="checkbox" name="toggle-4" id="toggle-4" class="btn-toggle">
                <label for="toggle-4"></label>
                <label class="toggle-button-label">Out-of-home mode</label>
                    <br />
                    <input type="checkbox" name="toggle-5" id="toggle-5" class="btn-toggle">
                <label for="toggle-5"></label>
                <label class="toggle-button-label">Ambient mode</label>
                    <br />
                    <input type="checkbox" name="toggle-6" id="toggle-6" class="btn-toggle">
                <label for="toggle-6"></label>
                <label class="toggle-button-label">Relax mode</label>
                    <br />
                    <input type="checkbox" name="toggle-7" id="toggle-7" class="btn-toggle">
                <label for="toggle-7"></label>
                <label class="toggle-button-label">Movie mode</label>
                    <br />
                    <input type="checkbox" name="toggle-8" id="toggle-8" class="btn-toggle">
                <label for="toggle-8"></label>
                <label class="toggle-button-label">Arduino Reset</label>
                    <br />
                    <input type="checkbox" name="toggle-9" id="toggle-9" class="btn-toggle btn-toggle-red">
                <label for="toggle-9"></label>
            </div>
	        
	        <div id="footer">
	           <p id="signature"><span class="letter-highlight">S</span>mart <span class="letter-highlight">H</span>ome <span class="letter-highlight">A</span>rduino <span class="letter-highlight">P</span>latfotm & <span class="letter-highlight">E</span>ngine</p>
	        </div>
	    </div>
	</body>
</html>
