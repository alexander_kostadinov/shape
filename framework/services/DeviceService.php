<?php

/**
 * Device Service Class
 * @author alexander.kostadinov
 *
 */
class DeviceService extends ResourceHandler
{
    private $resource;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }
    
    /**
     * Updates a device's state
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function updateDeviceState($params)
    {
        $this->getResource();
    
    
        $modelHandler = $this->resource->initResource('device', 'model');
        $this->resource->execute($modelHandler, 'updateDeviceState', $params);
    
        $result = $this->resource->getResponse();
    
        return $result;
    }
    
    /**
     * Gets a device's state
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getDeviceState($params)
    {
        $this->getResource();
    
    
        $modelHandler = $this->resource->initResource('device', 'model');
        $this->resource->execute($modelHandler, 'getDeviceState', $params);
    
        $result = $this->resource->getResponse();
    
        return $result;
    }
    
    /**
     * Updates the motion sensor last triggered time
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function updateMotionSensorLastTrigger($params) {
        $this->getResource();
    
    
        $modelHandler = $this->resource->initResource('device', 'model');
        $this->resource->execute($modelHandler, 'updateMotionSensorLastTrigger', $params);
    
        $result = $this->resource->getResponse();
    
        return $result;
    }
    
    /**
     * Adds supplementary device parameters 
     * (e.g. does the motion sensor trigger the led lamp)
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function addDeviceParams($params) {
        $this->getResource();
    
        $modelHandler = $this->resource->initResource('device', 'model');
        $this->resource->execute($modelHandler, 'addDeviceParams', $params);
    
        $result = $this->resource->getResponse();
    
        return $result;
    }
}

