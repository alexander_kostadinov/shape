<?php

/**
 * Login Service Class
 * @author alexander.kostadinov
 *
 */
class LoginService extends ResourceHandler
{
    private $resource;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    /**
     * Does login
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doLogin($params)
    {
        $this->getResource();


        $modelHandler = $this->resource->initResource('login', 'model');
        $this->resource->execute($modelHandler, 'doLogin', $params);

        $result = $this->resource->getResponse();

        return $result;
    }

    /**
     * Reads user info from state
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function readUserInfo($params)
    {   
        if (! empty($_SESSION['username']) && ! empty ($_SESSION['last_login'])) {
            
            return array(
                'username' => $_SESSION['username'], 
                'last_login' => $_SESSION['last_login'],
                'ip' => $_SESSION['ip']);
        }
    }

    /**
     * Does logout
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function doLogout($params)
    {
        $_SESSION = array();
        session_destroy();

        return array('hasLoggedOut' => true);
    }
}

