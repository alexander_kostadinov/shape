<?php

/**
 * Login Repository Class
 * @author alexander.kostadinov
 *
 */
class LoginRepository extends ResourceHandler
{
    const MASTER_USER_ID = 1;
    
    private $db;

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    /**
     * Gets user info
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getLoginUserInfo($params)
    {
        $this->getDb();
        $sql = 'SELECT * FROM USER AS u
                        WHERE u.id = ' . LoginRepository::MASTER_USER_ID .'
                        AND u.username = :username';

        $this->db->query($sql);
        $this->db->bind(':username', $params->username);
        $this->db->execute();
        return $this->db->single();
    }
    
    /**
     * Gets the last login time
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getLastLoginAndIp($params)
    {
        $this->getDb();
        $sql = 'SELECT last_login, ip FROM USER AS u
                       WHERE u.username = :username
                       AND u.id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->user_id);
        $this->db->bind(':username', $params->username);
        $this->db->execute();
        $result = $this->db->single();

        return $result;
    }
    
    /**
     * Gets the user hashes
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getHash($params)
    {
        $this->getDb();
        $sql = 'SELECT created_dt, last_dt FROM USER AS u
                       WHERE u.username = :username
                       AND u.id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':user_id', $params->user_id);
        $this->db->bind(':username', $params->username);
        $this->db->execute();
        $result = $this->db->single();

        return $result;
    }

    /**
     * Inserts a last login time
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function insertLastLoginIp($params)
    {
        $this->getDb();
        $sql = 'UPDATE USER AS u
                        SET
                        last_login = CURRENT_TIMESTAMP,
                        ip = :ip
                        WHERE u.username = :username
                        AND u.id = :user_id';

        $this->db->query($sql);
        $this->db->bind(':ip', $params->ip);
        $this->db->bind(':user_id', $params->user_id);
        $this->db->bind(':username', $params->username);
        $this->db->execute();

        return $this->db->lastInsertId();
    }
    
    /**
     * Updates one of the two hashes on choice
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function insertHash($params)
    {
        $this->getDb();
        
        $sql;
        
        if ($params->field == 'last_dt') {
            $sql = 'UPDATE USER AS u
                        SET u.last_dt = :value
                        WHERE u.username = :username
                        AND u.id = :user_id';
        } else {
            $sql = 'UPDATE USER AS u
                        SET u.created_dt = :value
                        WHERE u.username = :username
                        AND u.id = :user_id';
        }
        
        $this->db->query($sql);
        $this->db->bind(':value', $params->value);
        $this->db->bind(':user_id', $params->user_id);
        $this->db->bind(':username', $params->username);
        $this->db->execute();

        return $this->db->lastInsertId();
    }
}

