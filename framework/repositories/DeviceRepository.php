<?php

/**
 * Device Repository Class
 * @author alexander.kostadinov
 *
 */
class DeviceRepository extends ResourceHandler
{
    
    
    private $db;

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }
    
    /**
     * Gets device state
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getDeviceState($params)
    {
        $this->getDb();
        $sql = 'SELECT * FROM DEVICE AS d
                        WHERE d.id = :id
                        AND d.name = :device';
    
        $this->db->query($sql);
        $this->db->bind(':id', $params->id);
        $this->db->bind(':device', $params->device);
        $this->db->execute();
        
        return $this->db->single();
    }
    
    /**
     * Updates device state
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function updateDeviceState($params)
    {
        $this->getDb();
        $sql = 'UPDATE DEVICE AS d
                        SET
                        d.state = :state,
                        d.last_updated = CURRENT_TIMESTAMP,
                        d.is_active = 1
                        WHERE d.id = :id
                        AND d.name = :device';
    
        $this->db->query($sql);
        $this->db->bind(':state', $params->state);
        $this->db->bind(':id', $params->id);
        $this->db->bind(':device', $params->device);
        $this->db->execute();
    
        return $this->db->lastInsertId();
    }
    
    /**
     * Updates device previous update
     * (moves last_updated to previous_state_change)
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function updateDevicePreviousStateChange($params)
    {
        $this->getDb();
        $sql = 'UPDATE DEVICE AS d
                        SET
                        d.previous_state_change = :previous_state_change
                        WHERE d.id = :id
                        AND d.name = :device';
    
        $this->db->query($sql);
        $this->db->bind(':previous_state_change', $params->previous_state_change);
        $this->db->bind(':id', $params->id);
        $this->db->bind(':device', $params->device);
        $this->db->execute();
    
        return $this->db->lastInsertId();
    }
    
    /**
     * Updates other devices to be inactive
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function makeOtherDevicesInactive($params)
    {
        $this->getDb();
        $sql = 'UPDATE DEVICE AS d
                        SET
                        d.is_active = 0
                        WHERE d.id != :id
                        AND d.name != :device';
    
        $this->db->query($sql);
        $this->db->bind(':id', $params->id);
        $this->db->bind(':device', $params->device);
        $this->db->execute();
    
        return $this->db->lastInsertId();
    }
    
    /**
     * Updates the motion sensor last triggered time
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function updateMotionSensorLastTrigger($params) {
        $this->getDb();
        $sql = 'UPDATE DEVICE AS d
                        SET
                        d.last_updated = CURRENT_TIMESTAMP
                        WHERE d.id = :id
                        AND d.name = :device';
        
        $this->db->query($sql);
        $this->db->bind(':id', $params->id);
        $this->db->bind(':device', $params->device);
        $this->db->execute();
        
        return $this->db->lastInsertId();
    }
    
    /**
     * Adds supplementary device parameters
     * (e.g. does the motion sensor trigger the led lamp)
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function addDeviceParams($params) {
        $this->getDb();
        $sql = 'UPDATE DEVICE AS d
                        SET
                        d.additional_data = :additional_data
                        WHERE d.id = :id
                        AND d.name = :device';
    
        $this->db->query($sql);
        $this->db->bind(':additional_data', $params->additional);
        $this->db->bind(':id', $params->id);
        $this->db->bind(':device', $params->device);
        $this->db->execute();
    
        return $this->db->lastInsertId();
    }
}

