<?php

/**
 * Login Model Class
 * @author alexander.kostadinov
 *
 */
class LoginModel extends ResourceHandler
{
    //login error constants
    const EMPTY_USERNAME = 1;
    const EMPTY_PASSWORD = 2;
    const WRONG_CREDENTIALS = 3;

    //necessary resources for every class
    private $resource;
    private $db;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }

    /**
     * Checks for empty login parameters
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    private function checkForEmptyLoginParameters($params) {
        $errors = array();

        if (empty($params->username)) {
            $errors[self::EMPTY_USERNAME] = self::EMPTY_USERNAME;
        }

        if (empty($params->password)) {
            $errors[self::EMPTY_PASSWORD] = self::EMPTY_PASSWORD;
        }

        return $errors;
    }

    /**
     * Creates a hash
     * created_date must be a valid date() formatted string
     *
     * @param string $string to be hashed
     * @param string $hash_method - the hashing method
     *
     * $return string
     *
     * @author alexander.kostadinov
     */
    private function createHash($string, $hash_method = 'sha-512') {
        // the salt will be the reverse of the user's created date
        // in seconds since the epoch
        $salt = strrev(time());
        if (function_exists('hash') && in_array($hash_method, hash_algos())) {
            return hash($hash_method, $salt.$string);
        }

        return sha1($salt.$string);
    }

    /**
     * Checks a hash against corresponding diven timestamp
     * created_date must be a valid date() formatted string
     *
     * @param string $string to be hashed and tested
     * @param $timestamp to be used in the salt
     * @param string $hash_method - the hashing method
     *
     * $return string
     *
     * @author alexander.kostadinov
     */
    private function checkHash($string, $timestamp, $hash_method = 'sha-512') {
        // the salt will be the reverse of the user's created date
        // in seconds since the epoch
        $salt = strrev($timestamp);
        if (function_exists('hash') && in_array($hash_method, hash_algos())) {
            return hash($hash_method, $salt.$string);
        }

        return sha1($salt.$string);
    }

    /**
     * Gets the IP of the user that tries to log in
     *
     * $return string the ip of the user
     *
     * @author alexander.kostadinov
     */
    private function getIpAddress() {
        $ip = '';

        if (! empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }


    /**
     * Processes the login flow of events
     * @param object $params
     * @return array with errors|boolean
     * @author alexander.kostadinov
     */
    public function doLogin($params)
    {
        //check for empty credentials and return of errors
        $emptyCredentialsCheck = $this->checkForEmptyLoginParameters($params);

        if (! empty($emptyCredentialsCheck)) {

            return $emptyCredentialsCheck;
        }

        //set of resource and db getter and initialization of login repository (used for the whole method)
        $this->getResource();
        $this->getDb();

        $repoHandler = $this->resource->initResource('login', 'repository');

        //get user info
        $this->resource->execute($repoHandler, 'getLoginUserInfo', $params);
        $userInfo = $this->resource->getResponse();

        //validation: if username is right
        //or password is right
        //then refresh hashes and continue forward
        //else return wrong credentials error
        if ($userInfo['username'] == $params->username
                        && $userInfo['password'] == md5($params->password)) {

            //get current user hashes (both)
            $hashRepoParams = (object) array(
                    'username' => $userInfo['username'],
                    'user_id' => $userInfo['id']
            );

            $this->resource->execute($repoHandler, 'getHash', $hashRepoParams);
            $hashInfo = $this->resource->getResponse();

            //first update the last_dt hash to get the created_dt hash value
            //then renew the created_dt hash
            $hashRepoParamsGetLast = (object) array(
                    'field' => 'last_dt',
                    'value' => $hashInfo['created_dt'],
                    'username' => $userInfo['username'],
                    'user_id' => $userInfo['id']
            );

            $hashRepoParamsSetCreated = (object) array(
                    'field' => 'created_dt',
                    'value' => $this->createHash(APPLICATION_SECRET_KEY),
                    'username' => $userInfo['username'],
                    'user_id' => $userInfo['id']
            );

            $this->db->beginTransaction();
            try {
                $this->resource->execute($repoHandler, 'insertHash', $hashRepoParamsGetLast);
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage());
            }

            $this->db->beginTransaction();
            try {
                $this->resource->execute($repoHandler, 'insertHash', $hashRepoParamsSetCreated);
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage());
            }

        } else {

            return array(self::WRONG_CREDENTIALS => self::WRONG_CREDENTIALS);
        }

        //validation: if previous hash equals renewed hash
        //and previous hash does not correspond to the
        //last login date, return wrong credentials error
        if ($hashInfo['last_dt'] == $hashInfo['created_dt']
                        && $this->checkHash(APPLICATION_SECRET_KEY, $userInfo['last_login']) != $hashInfo['last_dt']) {

            return array(self::WRONG_CREDENTIALS => self::WRONG_CREDENTIALS);
        }

        //if all has passed well, set the user credentials in the state
        $_SESSION['reg']->state['username'] = $userInfo['username'];
        $_SESSION['reg']->state['user'] = $userInfo['visible_name'];

        $repoParams = (object) array(
                'username' => $userInfo['username'],
                'user_id' => $userInfo['id'],
                'ip' => $this->getIpAddress(),
                'created_dt' => $this->createHash(APPLICATION_SECRET_KEY),
                'last_dt' => $hashInfo['created_dt']
        );

        //set the last login time and ip with the current timestamp
        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'insertLastLoginIp', $repoParams);
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }

        $this->resource->execute($repoHandler, 'getLastLoginAndIp', $repoParams);
        $userLastLoginAndIp = $this->resource->getResponse();

        $_SESSION['reg']->state['last_login'] = $userLastLoginAndIp['last_login'];
        $_SESSION['reg']->state['ip'] = $userLastLoginAndIp['ip'];

        return array(
                'username' => $_SESSION['reg']->state['user']
        );
    }
}

