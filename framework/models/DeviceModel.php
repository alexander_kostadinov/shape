<?php

/**
 * Device Model Class
 * @author alexander.kostadinov
 *
 */
class DeviceModel extends ResourceHandler
{
    const DEVICE_MOTION = 'motion';
    const DEVICE_LED = 'led';
    const DEVICE_TEMP = 'temp';
    const DEVICE_MOTION_ID = '1';
    const DEVICE_LED_ID = '2';
    const DEVICE_TEMP_ID = '3';
    const REFERER_ARDUINO = 'arduino';
    const MODE_AMBIENT = 'ambient';
    const MODE_RELAX = 'relax';
    const MODE_MOVIE = 'movie';
    
    //necessary resources for every class
    private $resource;
    private $db;

    private function getResource()
    {
        $this->resource = $_SESSION['reg']->registry['resource'];
    }

    private function getDb()
    {
        $this->db = $_SESSION['reg']->registry['database'];
    }
    
    /**
     * Updates a device's state
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function updateDeviceState($params)
    {
        //set of resource and db getter and initialization of login repository (used for the whole method)
        $this->getResource();
        $this->getDb();
        
        switch($params->device) {
            case DeviceModel::DEVICE_MOTION:
                $cmd = new OnOffCommand($params->state);
                $item = new SwitchItem();
                $filteredCommand = $item->send($cmd)->filteredCommand;
                $params->state = ! is_array($filteredCommand) ? $filteredCommand : $filteredCommand['command'];
                $params->id = DeviceModel::DEVICE_MOTION_ID;
                break;
            case DeviceModel::DEVICE_LED:
                if (isset($params->predefined)) {
                    switch($params->state) {
                        case DeviceModel::MODE_AMBIENT:
                            $cmd = new RGBCommand(RGBCommand::COMMAND_YELLOW);
                            break;
                        case DeviceModel::MODE_MOVIE:
                            $cmd = new RGBCommand(RGBCommand::COMMAND_BLUE_MOVIE);
                            break;
                        case DeviceModel::MODE_RELAX:
                            $cmd = new RGBCommand(RGBCommand::COMMAND_GREEN_RELAX);
                            break;
                    }
                } else {
                    $cmd = new RGBCommand($params->state);
                }
                $item = new ColorItem();
                $filteredCommand = $item->send($cmd)->filteredCommand;
                $params->state = ! is_array($filteredCommand) ? $filteredCommand . '!' : $filteredCommand['command'] . '!';
                $params->id = DeviceModel::DEVICE_LED_ID;
                break;
            case DeviceModel::DEVICE_TEMP:
                //we use string command so that a '-' value for off (if apliccable) can be used
                $cmd = new StringCommand($params->state);
                $item = new StringItem();
                $filteredCommand = $item->send($cmd)->filteredCommand;
                $params->state = ! is_array($filteredCommand) ? $filteredCommand : $filteredCommand['command'];
                $params->id = DeviceModel::DEVICE_TEMP_ID;
                break;
        }
        
        $repoHandler = $this->resource->initResource('device', 'repository');

        $this->resource->execute($repoHandler, 'getDeviceState', $params);
        $deviceInfo = $this->resource->getResponse();
        
        $deviceParams = (object) array(
            'id' => $deviceInfo['id'],
            'device' => $deviceInfo['name'],
            'previous_state_change' => $deviceInfo['last_updated']
        );
        
        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'updateDevicePreviousStateChange', $deviceParams);
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }
        
        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'makeOtherDevicesInactive', $deviceParams);
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }
        
        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'updateDeviceState', $params);
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }
        
        $repoHandler = $this->resource->initResource('device', 'repository');
        $this->resource->execute($repoHandler, 'getDeviceState', $params);
        
        return $this->resource->getResponse();
    }
    
    /**
     * Gets a device's state
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function getDeviceState($params)
    {
         //set of resource and db getter and initialization of login repository (used for the whole method)
        $this->getResource();
        $this->getDb();
        
        switch($params->device) {
            case DeviceModel::DEVICE_MOTION:
                $params->id = DeviceModel::DEVICE_MOTION_ID;
                break;
            case DeviceModel::DEVICE_LED:
                $params->id = DeviceModel::DEVICE_LED_ID;
                break;
            case DeviceModel::DEVICE_TEMP:
                $params->id = DeviceModel::DEVICE_TEMP_ID;
                break;
        }
        
        $repoHandler = $this->resource->initResource('device', 'repository');

        $this->resource->execute($repoHandler, 'getDeviceState', $params);
        
        $response = $this->resource->getResponse();
        
        if ($params->device == DeviceModel::DEVICE_LED && isset($params->referer) && $params->referer == DeviceModel::REFERER_ARDUINO) {
            
            return trim($response['state'], '()!');
        }
        
        return $response;
    }
    
    /**
     * Updates the motion sensor last triggered time
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function updateMotionSensorLastTrigger($params) {
        $deviceParams = (object) array(
            'device' => DeviceModel::DEVICE_MOTION,
        );
        
        $currentDeviceData = $this->getDeviceState($deviceParams);
        
        if ($currentDeviceData['state'] && $currentDeviceData['state'] == 'ON') {
            $repoHandler = $this->resource->initResource('device', 'repository');
            $deviceUpdateParams = (object) array(
                    'id' => $currentDeviceData['id'],
                    'device' => $currentDeviceData['name'],
                    'previous_state_change' => $currentDeviceData['last_updated']
            );
            
            //sleep script so that Arduino data is correctly stored
            sleep(5);
            
            $this->db->beginTransaction();
            try {
                $this->resource->execute($repoHandler, 'updateDevicePreviousStateChange', $deviceUpdateParams);
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage());
            }
            
            $this->db->beginTransaction();
            try {
                $this->resource->execute($repoHandler, 'updateMotionSensorLastTrigger', $deviceUpdateParams);
                $this->db->commitTransaction();
            } catch (Exception $e) {
                $this->db->rollBack();
                throw new Exception($e->getMessage());
            }
        }
    }
    
    /**
     * Adds supplementary device parameters
     * (e.g. does the motion sensor trigger the led lamp)
     * @param object $params
     * @return array
     * @author alexander.kostadinov
     */
    public function addDeviceParams($params) {
        switch($params->device) {
            case DeviceModel::DEVICE_MOTION:
                $params->id = DeviceModel::DEVICE_MOTION_ID;
                break;
            case DeviceModel::DEVICE_LED:
                $params->id = DeviceModel::DEVICE_LED_ID;
                break;
            case DeviceModel::DEVICE_TEMP:
                $params->id = DeviceModel::DEVICE_TEMP_ID;
                break;
        }
        
        $currentDeviceData = $this->getDeviceState($params);
        
        $repoHandler = $this->resource->initResource('device', 'repository');
        $deviceUpdateParams = (object) array(
                'id' => $currentDeviceData['id'],
                'device' => $currentDeviceData['name'],
                'previous_state_change' => $currentDeviceData['last_updated']
        );
        
        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'updateDevicePreviousStateChange', $deviceUpdateParams);
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }
        
        $this->db->beginTransaction();
        try {
            $this->resource->execute($repoHandler, 'addDeviceParams', $params);
            $this->db->commitTransaction();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw new Exception($e->getMessage());
        }
    }
}

