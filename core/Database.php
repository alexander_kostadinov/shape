<?php
/**
 * Database mapper class
 * @author alexander.kostadinov
 *
 */
class Database
{
    //Definition of db configuration (constants come from Confiruration.php)
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    private $dbh;
    private $error;

    private $stmt;

    /**
     * Public constructor
     */
    public function __construct()
    {
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

        // Set options
        $options = array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        );

        // Create a new PDO instanace
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        } catch(PDOException $e){
            $this->error = $e->getMessage();
        }
    }

    /**
     * Prepares a statement
     * @author alexander.kostadinov
     */
    public function query($query)
    {
        $this->stmt = $this->dbh->prepare($query);
    }

    /**
     * Binds parameter (e.g. :name), value (e.g. John Doe) and type (e.g. string) to the statement
     *
     * @param string $param
     * @param string $value
     * @param string $type
     * @author alexander.kostadinov
     */
    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    /**
     * Binds all the given params with all values
     *
     * @param array $params
     * @param array $values
     * @author alexander.kostadinov
     */
    public function bindParams($params, $values)
    {
        if (! empty($params)) {
            foreach ($params as $key => $param) {
                $this->bind($param[$key], $values[$key]);
            }
        }
    }

    /**
     * Executes a prepared statement
     * @author alexander.kostadinov
     */
    public function execute()
    {
        return $this->stmt->execute();
    }

    /**
     * Returns an array of the result set rows
     * @author alexander.kostadinov
     */
    public function resultset()
    {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Returns a single record from the database
     * @author alexander.kostadinov
     */
    public function single()
    {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Returns the number of effected rows from the previous delete, update or insert statement
     * @author alexander.kostadinov
     */
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }


    /**
     * Returns the last inserted Id as a string
     * @author alexander.kostadinov
     */
    public function lastInsertId()
    {
        return $this->dbh->lastInsertId();
    }

    /**
     * Begins a transaction
     *@author alexander.kostadinov
     */
    public function beginTransaction()
    {
        return $this->dbh->beginTransaction();
    }

    /**
     * Commits a transaction
     *@author alexander.kostadinov
     */
    public function commitTransaction()
    {
        return $this->dbh->commit();
    }

    /**
     * Cancels a transaction
     *@author alexander.kostadinov
     */
    public function rollBack()
    {
        return $this->dbh->rollBack();
    }
     
    /**
     * Prevents unserializable objects from serialization (magic method)
     *
     * @author alexander.kostadinov
     */
    public function __sleep()
    {
        return array();
    }
}

