<?php

/**
 * Main resource handler class to be extended by services, models, repositories
 * @author Alexander Kostaidnov
 *
 * Example of usage:
 * $modelHandler = $this->initResource('exam', 'model');
 * $result = $this->execute($modelHandler, 'getUserName', array('userId' => 123));
 */
class ResourceHandler
{
    const PHP_FILE_EXTENSION = '.php';
    const RESOURCE_SERVICE_STRING = 'service';
    const RESOURCE_MODEL_STRING = 'model';
    const RESOURCE_REPOSITORY_STRING = 'repository';
    const RESOURCE_SERVICE = 1;
    const RESOURCE_MODEL = 2;
    const RESOURCE_REPOSITORY = 3;
    const APPLICATION_ROOT_PATH = APPLICATION_ROOT_PATH;

    const RESOURCE_PATH_SERVICE = 'framework/services/';
    const RESOURCE_PATH_MODEL = 'framework/models/';
    const RESOURCE_PATH_REPOSITORY = 'framework/repositories/';

    protected $response;

    /**
     * Initializes a resource
     * (creates an instance of a service, model or repository) by its name
     * e.g. ExamModel.php will be called with parameters exam ($name) & model ($type)
     *
     * @param string $name
     * @param string $type
     * @return object $resource - an instance of the service/model/repo class
     * @author Alexander Kostaidnov
     */
    public function initResource($name, $type)
    {
        if (! empty($name) && ! empty($type)) {
             
            $mappedType = $this->getResourceTypeLiteral($type);
             
            if (! $mappedType) {
                throw new Exception('initResource($name, $type) -> incorrect type!');
            }
             
            $name = ucfirst($name);
            $type = ucfirst($type);
            $resourceName = sprintf('%s%s', $name, $type);
             
            switch ($mappedType) {
                case self::RESOURCE_SERVICE:
                    $resourceFileSource = self::APPLICATION_ROOT_PATH . self::RESOURCE_PATH_SERVICE
                    . $resourceName . self::PHP_FILE_EXTENSION;
                    if (! file_exists($resourceFileSource)) {
                        throw new Exception('initResource($name, $type) -> incorrect resource name!');
                    }
                    break;
                case self::RESOURCE_MODEL:
                    $resourceFileSource = self::APPLICATION_ROOT_PATH . self::RESOURCE_PATH_MODEL
                    . $resourceName . self::PHP_FILE_EXTENSION;
                    if (! file_exists($resourceFileSource)) {
                        throw new Exception('initResource($name, $type) -> incorrect resource name!');
                    }
                    break;
                case self::RESOURCE_REPOSITORY:
                    $resourceFileSource = self::APPLICATION_ROOT_PATH . self::RESOURCE_PATH_REPOSITORY
                    . $resourceName . self::PHP_FILE_EXTENSION;
                    if (! file_exists($resourceFileSource)) {
                        throw new Exception('initResource($name, $type) -> incorrect resource name!');
                    }
                    break;
            }
             
            $resource = new $resourceName();

            return $resource;
        }
         
        throw new ResourceHandlerException('initResource($name, $type) -> one of the parameters is empty!');
    }

    /**
     * Returns the resource type literal
     *
     * @param int $type
     * @return int|boolean
     * @author alexander.kostadinov
     */
    private function getResourceTypeLiteral($type)
    {
        if ($type == self::RESOURCE_SERVICE_STRING) {
            return self::RESOURCE_SERVICE;
        }
         
        if ($type == self::RESOURCE_MODEL_STRING) {
            return self::RESOURCE_MODEL;
        }
         
        if ($type == self::RESOURCE_REPOSITORY_STRING) {
            return self::RESOURCE_REPOSITORY;
        }
         
        return false;
    }
    /**
     * Invokes a method from a given resource and returns its result
     *
     * @param object $resource - an instance of the resource
     * @param string $method - name of the method from the resource to be called
     * @param array $params - arguments for the called method
     * @return array
     * @author alexander.kostadinov
     */
    public function execute($resource, $method, $params)
    {
        if (! empty($resource) && ! empty($method) && ! empty($params)) {

            $result = call_user_func(array($resource, $method), $params);

            $this->response = $result;
            return;
        }
         
        throw new Exception('execute($resource, $method, $params) -> one of the parameters is empty!');
    }

    public function getResponse()
    {
        return $this->response;
    }
}

