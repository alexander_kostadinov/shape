<?php
date_default_timezone_set('Europe/Sofia');
mb_internal_encoding('UTF-8');

//Credentials running on localhost, MUST be different on prod.
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'smarthome');

define('APPLICATION_SECRET_KEY', sha1('alexIsprEtTycoOolL2'));

define('APPLICATION_ROOT_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR);
define('APPLICATION_ROOT_URL', str_replace($_SERVER['DOCUMENT_ROOT'], '', APPLICATION_ROOT_PATH));

