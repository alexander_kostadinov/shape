<?php

/**
 * State class to store the state of the user and other session information
 *
 * @author alexander.kostadinov
 */
class State
{
    protected $state;

    public function __construct()
    {
        $this->state = array();
    }

    public function getState()
    {
        return $this->state;
    }
}

