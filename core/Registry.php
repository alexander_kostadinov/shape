<?php
/**
 * Registry class for singleton instance of classes
 * @author alexander.kostadinov
 *
 */
class Registry
{
    private static $instance = null;

    /**
     * Gets or sets the registry class instance
     * @return $instance
     * @author alexander.kostadinov
     */
    public static function getInstance()
    {
        if(self::$instance === null) {
            self::$instance = new Registry();
        }

        return self::$instance;
    }


    /**
     * private constructor
     */
    private function __construct() {
    }

    /**
     * Disallow cloning
     */
    private function __clone() {
    }

    /**
     * Sets a record in the registry
     * @param string $key
     * @param string $value
     * @throws Exception
     * @author alexander.kostadinov
     */
    public function set($key, $value) {
        if (isset($this->registry[$key])) {
            throw new Exception("There is already an entry for key " . $key);
        }

        $this->registry[$key] = $value;
    }

    /**
     * Gets a record in the registry
     * @param string $key
     * @param string $value
     * @throws Exception
     * @author alexander.kostadinov
     */
    public function get($key) {
        if (! isset($this->registry[$key])) {
            throw new Exception("There is no entry for key " . $key);
        }

        return $this->registry[$key];
    }
}

