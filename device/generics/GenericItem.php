<?php

/**
 * Generic abstract class giving
 * a backbone for items
 * 
 * @author Alexander Kostadinov
 */
abstract class GenericItem {
    
    private $allowedCommands = array();

    public function send($command, $params = null){}
    
    private function checkAllowedCommands(object $command){}

}