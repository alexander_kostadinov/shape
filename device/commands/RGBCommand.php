<?php 

/**
 * A skeleton for a RGB (red, green, blue) command type
 *
 * @author Alexander Kostadinov
 */
class RGBCommand extends GenericType {
    
    const COMMAND_BLACK = '(0,0,0)';
    const COMMAND_WHITE = '(255,255,255)';
    const COMMAND_RED = '(255,0,0)';
    const COMMAND_GREEN = '(0,255,0)';
    const COMMAND_GREEN_RELAX = '(0,125,0)';
    const COMMAND_BLUE = '(0,0,255)';
    const COMMAND_BLUE_MOVIE = '(0,0,200)';
    const COMMAND_YELLOW = '(255,255,0)';
    const COMMAND_CYAN = '(0,255,255)';
    const COMMAND_MAGENTA = '(255,0,255)';
     
    private $command;

    public function __construct($command) {
        if ($this->checkAllowedFormats($command)) {
            $this->setCommand($command);
        } else {
            throw new UnallowedCommandFormatException('Command \'' . $command . '\' has an unallowed format.');
        }
    }
    
    /**
     * Checks if the applied command is in
     * the allowed format
     *
     * @param (string) $command
     *
     * @return boolean
     *
     * @author Alexander Kostadinov
     */
    private function checkAllowedFormats($command) {
        $regex = '^\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$^';
        $originalCommand = $command;
        $command = ltrim($command, '(');
        $command = rtrim($command, ')');
        $command = explode(',', $command);
        
        if(! empty($originalCommand) 
            && is_string($originalCommand)
            && preg_match($regex, $originalCommand)
            && isset($command[0])
            && ((int) $command[0] >= 0)
            && ((int) $command[0] <= 255)
            && isset($command[1])
            && ((int) $command[1] >= 0)
            && ((int) $command[1] <= 255)
            && isset($command[2])
            && ((int) $command[2] >= 0)
            && ((int) $command[2] <= 255)) {
            
            return true;
        }
        
        return false;
    }

    private function setCommand($command) {
        $this->command = $command;
    }

    public function getCommand() {

        return $this->command;
    }
}

