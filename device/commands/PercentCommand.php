<?php 

/**
 * A skeleton for a Percent command type
 *
 * @author Alexander Kostadinov
 */
class PercentCommand extends GenericType {

    const COMMAND_ZERO_PERCENT = '0';
    const COMMAND_FIFTY_PERCENT = '50';
    const COMMAND_HUNDRED_PERCENT = '100';
    
    private $command;

    public function __construct($command) {
        if ($this->checkAllowedFormats($command)) {
            $this->setCommand($command);
        } else {
            throw new UnallowedCommandFormatException('Command \'' . $command . '\' has an unallowed format.');
        }
    }

    /**
     * Checks if the applied command is in
     * the allowed format
     *
     * @param (string) $command
     *
     * @return boolean
     *
     * @author Alexander Kostadinov
     */
    private function checkAllowedFormats($command) {
        $command = (int) $command;
        
        if (! empty($command) 
            && is_numeric($command)
            && $command >= 0
            && $command <= 100) {
            
            return true;
        }
        
        return false;
    }

    private function setCommand($command) {
        $this->command = $command;
    }

    public function getCommand() {

        return $this->command;
    }
}

