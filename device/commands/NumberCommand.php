<?php

/**
 * A skeleton for a Number command type
 * 
 * @author Alexander Kostadinov
 */
class NumberCommand extends GenericType {

    private $command;
    
    public function __construct($command) {
        if ($this->checkAllowedFormats($command)) {
            $this->setCommand($command);
        } else {
            throw new UnallowedCommandFormatException('Command \'' . $command . '\' has an unallowed format.');
        }
    }
	
    /**
     * Checks if the applied command is in 
     * the allowed format
     *
     * @param (string) $command
     *
     * @return boolean
     * 
     * @author Alexander Kostadinov
     */
    private function checkAllowedFormats($command) {
        
        $command = (int) $command;
        
        if (! empty($command) && is_numeric($command)) {
            
            return true;
        }
		
        return false;
    }
    
    private function setCommand($command) {
        $this->command = $command;
    }
    
    public function getCommand() {
    
	    return $this->command;
    } 
}

