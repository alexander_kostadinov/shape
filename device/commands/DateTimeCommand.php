<?php 

/**
 * A skeleton for a DateTime command type
 *
 * @author Alexander Kostadinov
 */
class DateTimeCommand extends GenericType {

    private $command;

    public function __construct($command) {
        if ($this->checkAllowedFormats($command)) {
            $this->setCommand($command);
        } else {
            throw new UnallowedCommandFormatException('Command \'' . $command . '\' has an unallowed format.');
        }
    }

    /**
     * Checks if the applied command is in
     * the allowed format
     *
     * @param (string) $command
     *
     * @return boolean
     *
     * @author Alexander Kostadinov
     */
    private function checkAllowedFormats($command) {
        $regex = '^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|1‌​1)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468]‌​[048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(0‌​2)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02‌​)(-)(29)))(\s([0-1][0 -9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$^';
        
        if(! empty($command)
                        && is_string($command)
                        && preg_match($regex, $command)) {
        
            return true;
        }
        
        return false;
    }

    private function setCommand($command) {
        $this->command = $command;
    }

    public function getCommand() {

        return $this->command;
    }
}

