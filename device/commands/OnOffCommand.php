<?php 

/**
 * A skeleton for a OnOff command type
 *
 * @author Alexander Kostadinov
 */
class OnOffCommand extends GenericType {
    const COMMAND_ON = 'ON';
    const COMMAND_OFF = 'OFF';
    
    private $command;

    public function __construct($command) {
        if ($this->checkAllowedFormats($command)) {
            $this->setCommand($command);
        } else {
            throw new UnallowedCommandFormatException('Command \'' . $command . '\' has an unallowed format.');
        }
    }

    /**
     * Checks if the applied command is in
     * the allowed format
     *
     * @param (string) $command
     *
     * @return boolean
     *
     * @author Alexander Kostadinov
     */
    private function checkAllowedFormats($command) {
        if (! empty($command) 
            && is_string($command) 
            && ($command === OnOffCommand::COMMAND_ON || $command === OnOffCommand::COMMAND_OFF)) {
            
            return true;
        }
        
        return false;
    }

    private function setCommand($command) {
        $this->command = $command;
    }

    public function getCommand() {

        return $this->command;
    }
}

