<?php

/**
 * Command handler class
 * @author Alexander Kostadinov
 */
class CommandHandler {
    
    public $filteredCommand;
    
    public function __construct($command, $params) {
        if (is_null($params)) {
            
            $this->filteredCommand = $command->getCommand();
        } else {
            $this->filteredCommand = array('command' => $command->getCommand(), 'params' => $params);
        }
    }
}

