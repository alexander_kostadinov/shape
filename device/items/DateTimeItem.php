<?php

/**
 * A skeletom for a DateTime item
 * 
 * @author Alexander Kostadinov
 */
class DateTimeItem extends GenericItem {
    
    private static $allowedCommands = array('DateTimeCommand');
    
    public function __construct() {}
    
    /**
     * Sends a command to an item
     *
     * @param (CommandTypeInstance) $command
     * @return CommandHandler instance
     *
     * @author Alexander Kostadinov
     */
    public function send($command, $params = null) {
        $this->checkAllowedCommands($command);
        
        return new CommandHandler($command, $params);
    }
    
    /**
     * Checks if the applied command is allowed
     *
     * @param (CommandTypeInstance) $command
     *
     * @throws UnallowedCommandException
     *
     * @author Alexander Kostadinov
     */
    private function checkAllowedCommands($command) {
        
        $allowedCmdsFoundCount = 0;
        
        foreach(DateTimeItem::$allowedCommands as $allowedCmd) {
            if (get_class($command) == $allowedCmd) {
                $allowedCmdsFoundCount++;
            }
        }
        
        if($allowedCmdsFoundCount == 0) {
            throw new UnallowedCommandException('Unallowed command of type '
                                                . get_class($command) . ' for instance of class '
                                                . get_class($this));
        }
    }
}

