<?php

/**
 * A skeletom for a Switch item
 * 
 * @author Alexander Kostadinov
 */
class SwitchItem extends GenericItem {
    
    private static $allowedCommands = array('OnOffCommand');
    
    public function __construct() {}
    
    /**
     * Sends a command to an item
     *
     * @param (CommandTypeInstance) $command
     * @return CommandHandler instance
     *
     * @author Alexander Kostadinov
     */
    public function send($command, $params = null) {
        $this->checkAllowedCommands($command);
        
        return new CommandHandler($command, $params);
    }
    
    /**
     * Checks if the applied command is allowed
     *
     * @param (CommandTypeInstance) $command
     *
     * @throws UnallowedCommandException
     *
     * @author Alexander Kostadinov
     */
    private function checkAllowedCommands($command) {
        
        $allowedCmdsFoundCount = 0;
        
        foreach(SwitchItem::$allowedCommands as $allowedCmd) {
            if (get_class($command) == $allowedCmd) {
                $allowedCmdsFoundCount++;
            }
        }
        
        if($allowedCmdsFoundCount == 0) {
            throw new UnallowedCommandException('Unallowed command of type '
                                                . get_class($command) . ' for instance of class '
                                                . get_class($this));
        }
    }
}

