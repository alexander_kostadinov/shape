-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Време на генериране: 
-- Версия на сървъра: 5.5.27
-- Версия на PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `smarthome`
--

-- --------------------------------------------------------

--
-- Структура на таблица `device`
--

CREATE TABLE IF NOT EXISTS `device` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `state` varchar(128) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `previous_state_change` varchar(128) NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `additional_data` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Ссхема на данните от таблица `device`
--

INSERT INTO `device` (`id`, `name`, `state`, `last_updated`, `previous_state_change`, `is_active`, `additional_data`) VALUES
(1, 'motion', 'ON', '2015-11-07 18:11:13', '2015-11-07 20:11:09', 1, 'trigger_led'),
(2, 'led', '(0,0,0)!', '2015-11-07 17:30:45', '2015-01-05 15:15:26', 0, NULL),
(3, 'temp', '0', '2015-11-07 07:30:19', '2015-01-05 15:15:27', 0, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `visible_name` varchar(32) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_dt` varchar(128) NOT NULL,
  `last_dt` varchar(128) NOT NULL,
  `ip` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Ссхема на данните от таблица `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `visible_name`, `last_login`, `created_dt`, `last_dt`, `ip`) VALUES
(1, 'akostadinov', 'c99e240d9bcc22b8d84386dd333f6a13', 'Alex', '2015-11-07 17:23:18', 'd1d28ea9983c398726029cb4cdbf665a3ccad823', 'f61f7116dd3d8e12d0c3d34f4182c8132d34ddd7', '::1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
