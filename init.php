<?php
session_start();

if (defined('IS_VALID') && IS_VALID == true) {
    $token = bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
    $_SESSION['token'] = $token;
    header('Token: ' . $token);
}

//Require configuration provider and
//set include path used for __autoload()
require_once('core/Configuration.php');
set_include_path(dirname(__FILE__));

function __autoload($className) {
    $resourceDirs = array(
        '/core/', 
        '/core/Exception/', 
        '/device/commands/',
        '/device/exception/',
        '/device/generics/',
        '/device/handlers/',
        '/device/items/',
        '/framework/models/',
        '/framework/repositories/',
        '/framework/services/'
    );
    
    $resourcePath = str_replace("\\", "/", get_include_path());
    
    foreach ($resourceDirs as $dir) {
        $filename = $resourcePath . $dir . $className;
            if (is_readable($filename . '.php')) {
                include($filename . '.php');
                
                break;
            }
    }
}

//Create an instance of registry and add/set the resource and db objects
$registry = Registry::getInstance();
$registry->set('resource', new ResourceHandler());
$registry->set('database', new Database());
$registry->set('state', new State());
$_SESSION['reg'] = $registry;

//result, returned from the resourceHandler
$resource = $_SESSION['reg']->registry['resource'];

if (! empty($_POST['params'])) {
    $params = json_decode(stripslashes($_POST['params']));
    
    $token = substr(rtrim(trim($params->token),'r'), 0, 43);
    $safeToken = substr($_SESSION['token'], 0, 43);
    
    if ($token != $safeToken 
        || (! empty($params->arduinoToken) && $params->arduinoToken != sha512(APPLICATION_SECRET_KEY . '&leonardo'))) {
        die('There was an error.');
    }
    $serviceHandler = $resource->initResource($params->module, $params->unit);
    $resource->execute($serviceHandler, $params->method, $params->params);
    $result = $resource->getResponse();
    if (isset($result['username']) && isset($_SESSION['reg']->state['user']) 
        && $result['username'] == $_SESSION['reg']->state['user']) {
        $_SESSION['username'] = $_SESSION['reg']->state['user'];
        $_SESSION['last_login'] = $_SESSION['reg']->state['last_login'];
        $_SESSION['ip'] = $_SESSION['reg']->state['ip'];
        
        echo json_encode($result);
    } else {
        echo json_encode($result);
    }
}

if (! empty ($_GET['referer']) && $_GET['referer'] == 'arduino') {

    if ((! empty($_GET['token']) && $_GET['token'] != sha1(APPLICATION_SECRET_KEY . '&leonardo'))) {
        die('There was an error.');
    }

    if (! empty($_GET['module']) && ! empty($_GET['unit']) && ! empty($_GET['method'])) {
        $params = (object) array(
            'device' => $_GET['device'],
            'state' => $_GET['state'],
            'referer' => $_GET['referer']
        );
        
        $serviceHandler = $resource->initResource($_GET['module'], $_GET['unit']);
        $resource->execute($serviceHandler, $_GET['method'], $params);
        echo $resource->getResponse();
    }
}
