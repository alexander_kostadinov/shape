#Smart Home Arduino Platform and Engine

##Introduction
Smart Home Arduino Platform and Engine (SHAPE) is a combination of an online platform (web service) 
and Arduino-based engine for controlling home devices remotely at the touch of a hand.

##Architecture
In general the system consists of several modules coupled with each other to serve as the skeleton of the prototyped smarthome network.

Firstly, we have got a BACK-END part denoted to run on the server side and implement functionalities as follows:
    - Provisioning of an MVC framework structure that is used for secure and easy communication with the FRONT-END (core logic for end-to-end
      communication as well as secured database interaction);
    - Provisioning of a framework which is denoted to ease the control of devices (subset of classes for device handling, command formation and
      validation, device state manipulation and updates);

Secondly, we have got a FRONT-END part denoted to run on the client side and implement functionalities as follows:
    - Provisioning of a web mobile-friendly interface for easy and secure user log-in and device control/monitoring;
    - Provisioning of a communication with the BACK-END (so that all updates sent from the FRONT-END are processed by the BACK-END and are then
      reflected again in the FRONT-END);

Finally, we have got an ARDUINO part denoted to run on the Arduino microcontroller and implement functionalities as follows:
    - Provisioning of a secure mechanism for constant pinging to the BACK-END, responsible for immediate reflection of desired device
      states over the real physical items (motion sensor, RGB LED lamp, temperature/humidity sensor);
    - Provisioning of an internal implementation for handling commands that are sent to the devices (so that when a command sent by
      the FRONT-END is processed by the BACK-END and stored in the database, the ARDUINO extracts it and transfers it on to the physical device,
      e.g. turns the lamp on);