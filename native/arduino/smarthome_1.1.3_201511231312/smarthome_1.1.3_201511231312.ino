#include <SPI.h>
#include <Ethernet.h>
#include <PololuLedStrip.h>

//-----------------//
// GENERAL CONFIG  //
//-----------------//

//Ethernet settings
//created mac for and IP address for Arduino 
//(since localhost for PC is 192.168.1.3, the IP for
//Arduino can be anything besides *.*.*.3 and *.*.*.1 
//which is the default gateway)
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,154);
char server[] = "192.168.1.3";
EthernetClient client;

//Token & Auth
String token = "755c2942681d146a68a7b9b53bd7c152db170bbb";
String auth = "Authorization: Basic c21hcnRob21lX2FsZXgtaw==";

//LED configuration

// Create a ledStrip object and specify the pin it will use.
PololuLedStrip<10> ledStrip;

// Create a buffer for holding the colors (3 bytes per color).
#define LED_COUNT 60
rgb_color colors[LED_COUNT];

void setup() {
	//start the serial connection between computer and Arduino
	Serial.begin(115200);

	//start the Ethernet connection and the server:
	Ethernet.begin(mac, ip);
	Serial.println("Arduino server IP address: ");
	Serial.println(Ethernet.localIP());
}

void loop() {
	//MOTION SENSOR >>>
	//blink Arduino's LED when motion detected
	digitalWrite(13,digitalRead(12));

	String cmd;
  
	if(digitalRead(12) == HIGH) {
		cmd = "triggered";
		Serial.println("Motion sensor triggered.");

		// Connect to the server
		if (client.connect(server, 80)) {
			client.print("GET /shape/init.php?");
			client.println("referer=arduino&module=device&unit=service&method=updateMotionSensorLastTrigger&token=" + token + "&device=motion&state=" + cmd + "&");
			client.println("HTTP/1.1");
			client.println("Host: 192.168.1.3");
			client.println("Connection: close");
			client.println(); // Empty line
			client.println(); // Empty line
			client.stop();    // Closing connection to server
		} else {
			// If Arduino can’t connect to the server (your computer or web page)
			Serial.println("–> connection failed/n");
		}

		// Give the server some time to recieve the data and store it (200 ms).
		delay(200);
	} 
  
	//LED UPDATE >>>
	if (client.connect(server, 80)) {
		client.print("GET /shape/init.php?");
		client.println("referer=arduino&module=device&unit=service&method=getDeviceState&token=" + token + "&device=led&state=undef");
		client.println("HTTP/1.1");
		client.println("Host: 192.168.1.3");
		client.println("Connection: close");
		client.println(); // Empty line
		client.println(); // Empty line
   
		//Initialize necessary commands for reading and result parsing
		char c; 
    int charLength = 0;
		String data; 
		String red;
		String green;
		String blue;
    
    while (client.connected()) {
      while (client.available()) {
        c = client.read();
        data += c;
      }
      charLength = data.length();
    }

		int startBracket = data.indexOf('(');
    int endBracket = data.indexOf(')');
    int commaIndex = data.indexOf(',');
    int secondCommaIndex = data.indexOf(',', commaIndex+1);
    red = data.substring(startBracket+1, commaIndex);
    green = data.substring(commaIndex+1, secondCommaIndex);
    blue = data.substring(secondCommaIndex+1, endBracket);

		rgb_color color;
		//Red and Green are inversed due to the specific LED characteristics
		color.red = green.toInt();
		color.green = red.toInt();
		color.blue = blue.toInt();

		// Update the colors buffer.
		for(uint16_t i = 0; i < LED_COUNT; i++)
		{
			colors[i] = color;
		}

		// Write to the LED strip.
		ledStrip.write(colors, LED_COUNT); 
		 
		// close your end after the server closes its end
		Serial.print("Showing color: ");
		Serial.print(color.red);
		Serial.print(",");
		Serial.print(color.green);
		Serial.print(",");
		Serial.println(color.blue);
	}

	if (!client.connected()) {
		Serial.println("disconnecting.");
		client.stop();
	}
	
	delay(5000);
}
