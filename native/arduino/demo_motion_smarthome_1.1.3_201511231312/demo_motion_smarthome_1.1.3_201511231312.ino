#include <SPI.h>
#include <Ethernet.h>
#include <PololuLedStrip.h>

//-----------------//
// GENERAL CONFIG  //
//-----------------//

//Ethernet settings
//created mac for and IP address for Arduino 
//(since localhost for PC is 192.168.1.3, the IP for
//Arduino can be anything besides *.*.*.3 and *.*.*.1 
//which is the default gateway)
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,154);
char server[] = "193.107.36.200";
EthernetClient client;

//Token & Auth
String token = "755c2942681d146a68a7b9b53bd7c152db170bbb";
String auth = "Authorization: Basic c21hcnRob21lX2FsZXgtaw==";

//LED configuration

// Create a ledStrip object and specify the pin it will use.
PololuLedStrip<10> ledStrip;

// Create a buffer for holding the colors (3 bytes per color).
#define LED_COUNT 60
rgb_color colors[LED_COUNT];

void setup() {
	//start the serial connection between computer and Arduino
	Serial.begin(115200);

	//start the Ethernet connection and the server:
	Ethernet.begin(mac);
	Serial.println("Arduino server IP address: ");
	Serial.println(Ethernet.localIP());
}

void loop() {
	//MOTION SENSOR >>>
	//blink Arduino's LED when motion detected
	digitalWrite(13,digitalRead(12));

	String cmd;
  
	if(digitalRead(12) == HIGH) {
		Serial.println("Motion sensor triggered.");

		// Connect to the server
		if (client.connect(server, 80)) {
			client.print("GET http://sandroid.info/master/init.php?");
			client.println("referer=arduino&module=device&unit=service&method=updateMotionSensorLastTrigger&token=" + token + "&device=motion&state=triggered");
			client.println("HTTP/1.1");
			client.println("Host: http://sandroid.info/master");
			client.println("Connection: close");
			client.println(); // Empty line
			client.println(); // Empty line
			client.stop();    // Closing connection to server
		} else {
			// If Arduino can’t connect to the server (your computer or web page)
			Serial.println("–> connection failed/n");
		}

		// Give the server some time to recieve the data and store it (200 ms).
		delay(200);
	} 
  
	if (!client.connected()) {
		Serial.println("disconnecting.");
		client.stop();
	}
}
