#include <SPI.h>
#include <Ethernet.h>
#include <PololuLedStrip.h>
#include <dht.h>

//-----------------//
// GENERAL CONFIG  //
//-----------------//

//Ethernet settings
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192, 168, 1, 154);
char server[] = "193.107.36.200";
EthernetClient client;

//Token & Auth
String token = "755c2942681d146a68a7b9b53bd7c152db170bbb";

//LED configuration
// Create a ledStrip object and specify the pin it will use.
PololuLedStrip<10> ledStrip;

// Create a buffer for holding the colors (3 bytes per color).
#define LED_COUNT 60
rgb_color colors[LED_COUNT];

//DHT11 Config
dht DHT;
#define DHT11_PIN A4

void setup() {
  //start the serial connection between computer and Arduino
  Serial.begin(115200);

  //start the Ethernet connection and the server:
  Ethernet.begin(mac);
  Serial.println("Arduino server IP address: ");
  Serial.println(Ethernet.localIP());
}

void loop() {
  //MOTION SENSOR >>>
  //blink Arduino's LED when motion detected
  digitalWrite(13, digitalRead(12));

  String cmd;

  if (digitalRead(12) == HIGH) {
    Serial.println("Motion sensor triggered.");

    // Connect to the server
    if (client.connect(server, 80)) {
      client.print("GET http://sandroid.info/master/init.php?");
      client.println("referer=arduino&module=device&unit=service&method=updateMotionSensorLastTrigger&token=" + token + "&device=motion&state=triggered");
      client.println("HTTP/1.1");
      client.println("Host: http://sandroid.info/master");
      client.println("Connection: close");
      client.println(); // Empty line
      client.println(); // Empty line
      client.stop();    // Closing connection to server
    } else {
      // If Arduino can’t connect to the server (your computer or web page)
      Serial.println("Connection failed!\n");
    }
  }

  //TEMP & HUMIDITY SENSOR >>>
  int chk = DHT.read11(DHT11_PIN);
  int h = DHT.humidity;
  int t = DHT.temperature;
  Serial.println(h);
  Serial.println(t);
  String dhtSensorState =  (String) t + "/" + (String) h;
  // Connect to the server
  if (client.connect(server, 80)) {
    client.print("GET http://sandroid.info/master/init.php?");
    client.println("referer=arduino&module=device&unit=service&method=updateDeviceState&token=" + token + "&device=temp&state=" + dhtSensorState);
    client.println("HTTP/1.1");
    client.println("Host: http://sandroid.info/master");
    client.println("Connection: close");
    client.println(); // Empty line
    client.println(); // Empty line
    client.stop();    // Closing connection to server
  } else {
    // If Arduino can’t connect to the server (your computer or web page)
    Serial.println("Connection failed!\n");
  }

  // Give the server some time to recieve the data and store it (200 ms).
  delay(10000);

  if (!client.connected()) {
    Serial.println("disconnecting.");
    client.stop();
  }
}
