#include <SPI.h>
#include <Ethernet.h>

//Ethernet settings
//created mac for and IP address for Arduino 
//(since localhost for PC is 192.168.1.4, the IP for
//Arduino can be anything besides *.*.*.4 and *.*.*.1 
//which is the default gateway)
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,154);
char server[] = "192.168.1.4";
EthernetClient client;

//token
String token = "755c2942681d146a68a7b9b53bd7c152db170bbb";


void setup() {
  Serial.print("Some text. ");
  //start the serial connection between computer and Arduino
  Serial.begin(115200);
  
  //start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  Serial.println("Arduino server IP address: ");
  Serial.println(Ethernet.localIP());
}

void loop() {
  //blink Arduino's LED when motion detected
  digitalWrite(13,digitalRead(12));
  
  String cmd;
  
  if(digitalRead(12) == HIGH) {
    cmd = "triggered";
  
    // Connect to the server
    if (client.connect(server, 80)) {
      client.print("GET /shape/init.php?");
      client.print("referer=arduino&module=device&unit=service&method=updateMotionSensorLastTrigger&token=" + token + "&device=motion&state=" + cmd + "&");
      client.println("HTTP/1.1");
      client.println("Host: 192.168.1.4");
      client.println("Connection: close");
      client.println(); // Empty line
      client.println(); // Empty line
      client.stop();    // Closing connection to server
    } else {
      // If Arduino can’t connect to the server (your computer or web page)
      Serial.println("–> connection failed/n");
    }
    
    // Give the server some time to recieve the data and store it (200 ms).
    delay(200);
  } 
}
