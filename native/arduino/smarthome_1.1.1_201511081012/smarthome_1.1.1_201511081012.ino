#include <SPI.h>
#include <Ethernet.h>
#include <PololuLedStrip.h>

//Ethernet settings
//created mac for and IP address for Arduino 
//(since localhost for PC is 192.168.1.4, the IP for
//Arduino can be anything besides *.*.*.4 and *.*.*.1 
//which is the default gateway)
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,154);
char server[] = "192.168.1.4";
EthernetClient client;

//token
String token = "755c2942681d146a68a7b9b53bd7c152db170bbb";

//LED configuration

// Create a ledStrip object and specify the pin it will use.
PololuLedStrip<10> ledStrip;

// Create a buffer for holding the colors (3 bytes per color).
#define LED_COUNT 60
rgb_color colors[LED_COUNT];

String readString = String(30);

void setup() {
  Serial.print("Some text. ");
  //start the serial connection between computer and Arduino
  Serial.begin(115200);
  
  //start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  Serial.println("Arduino server IP address: ");
  Serial.println(Ethernet.localIP());
}

void loop() {
  //blink Arduino's LED when motion detected
  digitalWrite(13,digitalRead(12));
  
  String cmd;
  
  if(digitalRead(12) == HIGH) {
    cmd = "triggered";
  
    // Connect to the server
    if (client.connect(server, 80)) {
      client.print("GET /shape/init.php?");
      client.print("referer=arduino&module=device&unit=service&method=updateMotionSensorLastTrigger&token=" + token + "&device=motion&state=" + cmd + "&");
      client.println("HTTP/1.1");
      client.println("Host: 192.168.1.4");
      client.println("Connection: close");
      client.println(); // Empty line
      client.println(); // Empty line
      client.stop();    // Closing connection to server
    } else {
      // If Arduino can’t connect to the server (your computer or web page)
      Serial.println("–> connection failed/n");
    }
    
    // Give the server some time to recieve the data and store it (200 ms).
    delay(200);
  } 
  
  // Connect to the server for LED
  if (client.connect(server, 80)) {
      client.print("GET /shape/init.php?");
      client.print("referer=arduino&module=device&unit=service&method=getDeviceState&token=" + token + "&device=led&state=undef");
      client.println("HTTP/1.1");
      client.println("Host: 192.168.1.4");
      client.println("Connection: close");
      client.println(); // Empty line
      client.println(); // Empty line
      
      String data;
      int commaNumber = 0;
      String red;
      String green;
      String blue;
      
      while(client.connected()) {
       // the server will close the connection when it is finished sending packets
       while(client.available()) {
         // ...but not until this buffer is empty
         char c = client.read();
         data += c;
        
       }
     }
     
     String pure;
     
     for (int i = 0; i < data.length(); i++) {
       if (isDigit(data[i])) {
          pure += data[i]; 
          continue;
       } else {
          if ((String) data[i] == ",") {
            commaNumber++;
            continue;
          }

          if (commaNumber == 1) {
            red = pure;
            pure = "";
            continue;
          }
  
          if (commaNumber == 2) {
            green = pure;
            commaNumber = 0;
            pure = "";
            continue;
          }
  
          if ((String) data[i] == ")") {
            blue = pure;
            pure = "";
            continue;
          }
        }
    }

    rgb_color color;
    color.red = red.toInt();
    color.green = green.toInt();
    color.blue = blue.toInt();

    // Update the colors buffer.
    for(uint16_t i = 0; i < LED_COUNT; i++)
    {
      colors[i] = color;
    }

    // Write to the LED strip.
    ledStrip.write(colors, LED_COUNT);  
   // close your end after the server closes its end
   Serial.print("Showing color: ");
   Serial.print(color.red);
   Serial.print(",");
   Serial.print(color.green);
   Serial.print(",");
   Serial.println(color.blue);
   
   client.stop();
  }

  // Give the server some time to recieve the data and store it (200 ms).
  delay(3000);
}
